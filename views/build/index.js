$(function() {
	var id = getUrlParam("id");

	function pagelist() {
		$.ajax({
			type: "get",
			url: BASE_URL+"/building/detailsMore",
			data: {
				"id": id
			},
			xhrFields: {
				withCredentials: true
			},
			success: function(data) {
				var html = "";
				var str = "";
				html += '<div class="pcontainer">' +
					'<h3 class="lp_name">' + data.data.buildingTitle + '</h3>' +
					'<ul><li class="ptese">' +
					'</li></ul>' +
					'<ul class="detail-info">' +
					'<li class="info"><label>销售情况：</label><span>' + data.data.buildingStateCn + '</span></li>' +
					'<li class="info"><label>楼盘地址：</label><span>' + data.data.buildingAddress + '</span></li>' +
					'<li class="info"><label>物业类型：</label><span>' + data.data.propertyTypeCn + '</span></li>' +
					'</ul>' +
					'<h3 class="lp_name">销售信息</h3>' +
					'<ul class="detail-info">' +
					'<li class="info"><label>最新开盘：</label><span>' + data.data.opDate + '</span></li>' +
					'<li class="info"><label>入住时间：</label><span>' + data.data.checkInDate + '</span></li>' +
					'<li class="info"><label>楼盘价格：</label><span>' + data.data.buildingPrice + '</span></li>' +
					'</ul>' +
					'<h3 class="lp_name">建筑规划</h3>' +
					'<ul class="detail-info">' +
					'<li class="info"><label>装修状况：</label><span>' + data.data.decorateStateCn + '</span></li>' +
					'<li class="info"><label>容积率：</label><span>' + data.data.plotRatio + '</span></li>' +
					'<li class="info"><label>绿化率：</label><span>' + data.data.greeningRate + '</span></li>' +
					'<li class="info"><label>规划户数：</label><span>' + data.data.households + '</span></li>' +
					'<li class="info"><label>规划面积：</label><span>' + data.data.planArea + '</span></li>' +
					'<li class="info"><label>建筑面积：</label><span>' + data.data.coveredArea + '</span></li>' +
					'</ul>' +
					'<h3 class="lp_name">物业信息</h3>' +
					'<ul class="detail-info">' +
					'<li class="info"><label>物业公司：</label><span>' + data.data.propertyCompany + '</span></li>' +
					'<li class="info"><label>物业管理费：</label><span>' + data.data.propertyCharge + '</span></li>' +
					'<li class="info"><label>车位数：</label><span>' + data.data.numberOfFloors + '</span></li>' +
					'</ul>' +
					'<h3 class="lp_name">楼盘信息</h3>' +
					'<ul class="detail-info">' +
					'<li class="info"><label>开发商：</label><span>' + data.data.developer + '</span></li>' +
					'<li class="info"><label>产权：</label><span>' + data.data.propertyRight + '</span></li>' +
					'<li class="info"><label>预售许可证：</label><span>' + data.data.permitForPresale + '</span></li>' +
					'</ul></div>';
				//					for(var j = 0; j < data.data.list.length; i++) {
				//						var result = data.data.list[i];
				//						str += '<a><span>' + result + '</span></a>'
				//					}
				//					$(".ptese").html(str);
				html = html.replace(/null/g,"暂无数据") 
				$(".content").html(html);
				
			}
		});
	};
	pagelist();
	$(".icon-right-home").click(function() {
		window.location.href = "../index/index.html";
	});
	$(".xiaoyuhao").click(function() {
		history.back();
	});

});