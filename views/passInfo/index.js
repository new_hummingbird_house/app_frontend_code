$(function() {
	var customId = getUrlParam("id");

	function verify() {

		$.ajax({
			type: "get",
			url: BASE_URL + "/voucherApplication/getById",
			data: {
				"id": customId
			},
			xhrFields: {
				withCredentials: true
			},
			success: function(data) {
				var html = "";
				var ele = data.data;
				html += [
					//客户姓名信息
					(function() {
						return '<div class="custom-subinfo">' +
							'<div class="pass-item" id="">' +
							'<span id="">姓名:</span>' +
							'<input type="text" name="name" id="name" value="' + ele.name + '" readonly="readonly"/>' +
							'</div>'
					})(),
					//客户电话
					(function() {
						return '<div class="pass-item" id="">' +
							'<span id="">电话:</span>' +
							'<input type="text" name="phone" id="phone" value="' + ele.phone + '" readonly="readonly"/>' +
							'</div>'
					})(),
					//客户身份证件号码
					(function() {
						return '<div class="pass-item" id="">' +
							'<span id="">身份证号:</span>' +
							'<input type="text" name="IDcode" id="IDcode" value="' + ele.identityCard + '" readonly="readonly" />' +
							'</div>'
					})(),
					//客户意向楼盘
					(function() {
						return '<div class="pass-item" id="">' +
							'<span id="">楼盘名称:</span>' +
							'<input type="text" name="buildName" id="buildName" value="' + ele.buildingTitle + '" readonly="readonly" />' +
							'</div>'
					})(),
					//客户房屋信息
					(function() {
						return '<div class="pass-item" id="">' +
							'<span id="">房屋信息:</span>' +
							'<input type="text" name="buildingInfo" id="buildingInfo" value="' + ele.apartment + '" readonly="readonly" />' +
							'</div>'
					})(),
					//兑换商品信息
					(function() {
						return '<div class="pass-item" id="">' +
							'<span id="">商品信息:</span>' +
							'<textarea rows="2" name="buildingInfo" id="buildingInfo" readonly="readonly" >'+ele.productName+'</textarea>' +
							'</div>'
					})(),						
					//状态
					(function() {

						return '<div class="pass-item blanking" id="">' +
							'<span id="">状态:</span>' +
							'<input type="text" name="state" class="status" id="' + ele.status + '" value="" readonly="readonly" />' +
							'</div>' +
							'</div>'

					})(),
					(function() {
						return '<div class="passBtn">' +
						'<button class="notPass btn " id="1">通过</button>' +
						'<button class="pass btn " id="2">拒绝</button>' +
						'</div>'
					})(),
				].join('')

				$(".custom").html(html);

				if($(".status").attr("id") == 0) {
					$(".status").val("待审核");
				} else if($(".status").attr("id") == 1) {
					$(".status").val("审核通过");
					$(".passBtn").addClass("lg-hide");
				} else {
					$(".status").val("审核未通过");
					$(".passBtn").addClass("lg-hide");
				}
			}
		});
	}
	verify();

	$(".custom").on("click",".notPass",function(){
		var notPass = $(".notPass").attr("id");
		var phone = $("#phone").val();
		$.ajax({
			type:"get",
			url:BASE_URL +"/voucherApplication/editByCheck",
			data:{
				"id":customId,
				"status":notPass,
				"phone":phone
			},
			xhrFields: {
				withCredentials: true
			},
			success: function(data){
				history.back();
			}
		});
	});
	$(".custom").on("click",".pass",function(){
		var pass = $(".pass").attr("id");
		var phone = $("#phone").val();
		$.ajax({
			type:"get",
			url:BASE_URL +"/voucherApplication/editByCheck",
			data:{
				"id":customId,
				"status":pass,
				"phone":phone
			},
			xhrFields: {
				withCredentials: true
			},
			success: function(data){
				history.back();
			}
		});
	});
	
	$(".xiaoyuhao").click(function() {
		history.back();
	});
})