$(function() {

	$.ajax({
		type: "get",
		url: BASE_URL + "/staff/reportDict",
		data: {

		},
		dataType: "json",
		xhrFields: {
			withCredentials: true
		},
		success: function(data) {
			var str1 = "";
			var str2 = "";

			for(var i = 0; i < data.data.buildingList.length; i++) {
				str1 += '<option value="' + data.data.buildingList[i].id + '">' + data.data.buildingList[i].buildingTitle + '</option>';
			};
			for(var j = 0; j < data.data.commendDict.length; j++) {
				str2 += '<option value="' + data.data.commendDict[j].value + '">' + data.data.commendDict[j].name + '</option>';
			};
			$("#loupan").append(str1);
			$("#goufang").html(str2);
		}

	});

	function guwen() {
		var loupan = $("#loupan").val();
		$.ajax({
			type: "get",
			url: BASE_URL + "/staff/adviserByBId",
			data: {
				"buildingId": loupan
			},
			dataType: "json",
			xhrFields: {
				withCredentials: true
			},
			success: function(data) {
				var str = "";

				for(var i = 0; i < data.data.length; i++) {
					str += '<option value="' + data.data[i].id + '">' + data.data[i].name + '</option>';
				}
				$("#guwen").html(str);
			}
		})
	}
	$("#loupan").change(function(){
		guwen();
		if($("#loupan").val() == ""){
			$("#guwen").val("");
		}
	});
	$(".nav-bar-left").click(function() {
		history.back();
	});
	$("#lg-sub-s").click(function() {
		var name = $("#name").val();
		var phone = $("#phone").val();
		var standbyPhone = $("#standbyPhone").val();
		var word = $("#words").val();
		var loupan = $("#loupan").val();
		var goufang = $("#goufang").val();
		var guwen = $("#guwen").val();

		function isPhoneNo(phone) {
			var pattern = /^1[34578]\d{9}$/;
			return pattern.test(phone);
		}
		if(name == "") {
			alert("姓名不能为空");
			return false;
		}
		if(phone == "") {
			alert("电话不能为空");
			return false;
		}
		if(isPhoneNo($.trim(phone)) == false) {
			alert("手机号码不正确");
			return false;
		}

		if(loupan == "") {
			alert("请选择意向楼盘");
			return false;
		}
		if(goufang == "") {
			alert("请选择购房意向");
			return false;
		}
		if(guwen == "") {
			alert("请选择置业顾问");
			return false;
		}
		$.ajax({
			type: "post",
			url: BASE_URL + "/staff/report",
			data: {
				"realName": name,
				"phone": phone,
				"sparePhone": standbyPhone,
				"buildingInfoId": loupan,
				"remark": word,
				"demand": goufang,
				"adviserId": guwen
			},
			dataType: "json",
			xhrFields: {
				withCredentials: true
			},
			success: function(data) {
				if(data.code == 200) {
					alert(data.msg);
					history.go(-1);
				} else {
					alert(data.msg);
				}
			}
		});
	})
});