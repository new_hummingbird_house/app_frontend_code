$(function () {
    var id = getUrlParam("id");
	function addBuildingApartment(buildingApartment){
		var str =  ' <div class="hx-list" id="hx-list" >'+
			'<div class="hx-img-wrap" class="mui-content-padded"  href="javascript:void(0)" data-magnify="gallery" data-group="g1" data-src="' + IMG_URL+ buildingApartment.apartmentImg + '"> ' +
				'<img class = "hx-img" id = "hx-img"  src="' + IMG_URL+ buildingApartment.apartmentImg + '">' +
				'<div class="huxinginfo">' +
					'<div class="hx-type">' +
						'<span class="hx-alias">' +
						buildingApartment.room + '室' + buildingApartment.hall + '厅' +
						buildingApartment.hutch + '卫 &nbsp;&nbsp;' + 
						(function(){
							if(buildingApartment.coveredArea==null){
								return ""
							}else{
								return buildingApartment.coveredArea + 'm²'
							}
						})()
						 +
						'</span>' +
						'<span class="hx-status" id="hx-status">' +
						(function(){
							if(buildingApartment.apartmentStateCn==null){
								return ""
							}else{
								return buildingApartment.apartmentStateCn 
							}							
						})()
						+
						'</span>' +
					'</div>' +
            	'<span class="area-name">' +
				(function(){
					if(buildingApartment.apartmentName==null){
						return "暂无数据"
					}else{
						return buildingApartment.apartmentName + '户型' 
					}							
				})()
				+
            	'</span>' +
            	'<span class="g-overflow-price" id="g-overflow-price">' +
							(function(){
								if(buildingApartment.apartmentPrice==null){
									return ""
								}else{
									return  buildingApartment.apartmentPrice +
                '元/m<sup>2</sup>'
								}
							})()+
           		'</span></div>' +
			'</div>'+
		'</div>';
		return str;
	}
    $.ajax({
        url: BASE_URL+"/building/apartment",
        data: {
            "id": id
        },
        xhrFields: {
			withCredentials: true
		},
        type: "get",
        dataType: "json",
        success: function (data) {
			var data = data.data;

            var apartmentStr1 = "";
			var apartmentStr2 = "";
			var apartmentStr3 = "";
			var apartmentStr4 = "";
			var apartmentStr5 = "";
			var apartmentStr6 = "";
			var apartmentStr7 = "";
			var apartmentStr8 = "";			
			
            /*循环列表*/
            var buildingApartmentList = data;
						
            if (buildingApartmentList != null) {
                for (var i = 0; i < buildingApartmentList.length; i++) {
                    var buildingApartment = buildingApartmentList[i];
					var room = buildingApartment.room;
					if(room==1){
						apartmentStr1 = apartmentStr1 + addBuildingApartment(buildingApartment);
					} else if(room==2){
						apartmentStr2 = apartmentStr2 + addBuildingApartment(buildingApartment);
					} else if(room==3){
						apartmentStr3 = apartmentStr3 + addBuildingApartment(buildingApartment);
					} else if(room==4){
						apartmentStr4 = apartmentStr4 + addBuildingApartment(buildingApartment);
					} else if(room==5){
						apartmentStr5 = apartmentStr5 + addBuildingApartment(buildingApartment);
					} else if(room==6){
						apartmentStr6 = apartmentStr6 + addBuildingApartment(buildingApartment);
					} else if(room==7){
						apartmentStr7 = apartmentStr7 + addBuildingApartment(buildingApartment);
					} else if(room>=8){
						apartmentStr8 = apartmentStr8 + addBuildingApartment(buildingApartment);
					}
                }
				if(apartmentStr1==''){
					$("#room-1").hide();
				}else{
					$("#room-1").show();
					$("#huxing-1").html(apartmentStr1)
					
				}
				if(apartmentStr2==''){
					$("#room-2").hide();
				}else{
					$("#room-2").show();
					$("#huxing-2").html(apartmentStr2)
				}
				if(apartmentStr3==''){
					$("#room-3").hide();
				}else{
					$("#room-3").show();
					$("#huxing-3").html(apartmentStr3)
				}
				if(apartmentStr4==''){
					$("#room-4").hide();
				}else{
					$("#room-4").show();
					$("#huxing-4").html(apartmentStr4)
				}
				if(apartmentStr5==''){
					$("#room-5").hide();
				}else{
					$("#room-5").show();
					$("#huxing-5").html(apartmentStr5)
				}
				if(apartmentStr6==''){
					$("#room-6").hide();
				}else{
					$("#room-6").show();
					$("#huxing-6").html(apartmentStr6)
				}
				if(apartmentStr7==''){
					$("#room-7").hide();
				}else{
					$("#room-7").show();
					$("#huxing-7").html(apartmentStr7)
				}
				if(apartmentStr8==''){
					$("#room-8").hide();
				}else{
					$("#room-8").show();
					$("#huxing-8").html(apartmentStr8)
				}
				$('[data-magnify]').Magnify();
				// yulan();
			}
        }
    });
		$('[data-magnify]').Magnify({
			Toolbar: [
				'prev',
				'next',
				'rotateLeft',
				'rotateRight',
				'zoomIn',
				'actualSize',
				'zoomOut'
			],
			keyboard:true,
			draggable:true,
			movable:true,
			modalSize:[800,600],
			beforeOpen:function (obj,data) {
				console.log('beforeOpen')
			},
			opened:function (obj,data) {
				console.log('opened')
			},
			beforeClose:function (obj,data) {
				console.log('beforeClose')
			},
			closed:function (obj,data) {
				console.log('closed')
			},
			beforeChange:function (obj,data) {
				console.log('beforeChange')
			},
			changed:function (obj,data) {
				console.log('changed')
			}
		});
	$(".xiaoyuhao").click(function() {
		history.back();
	});
// 	function yulan(){
// 		//图片预览
// 		mui.plusReady(function() {
// 		  // 获取图片地址列表
// 		  var images = document.querySelectorAll('img');
// 		  var urls = [];
// 		  for(var i = 0; i < images.length; i++) {
// 		    urls.push(images[i].src);
// 		  }
// 		  // 监听图片的点击
// 		  mui('body').on('tap', 'img', function() {
// 		    // 查询图片在列表中的位置
// 		    // 由于dom节点列表是伪数组，需要处理一下
// 		    var index = [].slice.call(images).indexOf(this);
// 		    plus.nativeUI.previewImage(urls, {
// 		      current: index,
// 		      loop: true,
// 		      indicator: 'number'
// 		    });
// 		  });
// 		});
// 	}

});



