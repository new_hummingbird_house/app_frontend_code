$(function () {
	guwen();
	$.ajax({
		type: "get",
		url: BASE_URL + "/staff/reportDict",
		data: {

		},
		dataType: "json",
		xhrFields: {
			withCredentials: true
		},
		success: function (data) {
			var str1 = "";
			var str2 = "";

			for (var i = 0; i < data.data.buildingList.length; i++) {
				str1 += '<option value="' + data.data.buildingList[i].id + '">' + data.data.buildingList[i].buildingTitle + '</option>';
			};
			for (var j = 0; j < data.data.commendDict.length; j++) {
				str2 += '<option value="' + data.data.commendDict[j].value + '">' + data.data.commendDict[j].name + '</option>';
			};
			$("#loupan").append(str1);
			$("#goufang").html(str2);
		}

	});

	function guwen() {
		var loupan = $("#loupan").val();
		$.ajax({
			type: "get",
			url: BASE_URL + "/staff/findAllAdviser",
			data: {},
			dataType: "json",
			xhrFields: {
				withCredentials: true
			},
			success: function (data) {
				if (data.code == '200') {
					var str = "";
					var adv = "";
					var result = data.data;
					for (var i = 0; i < result.length; i++) {
						str += '<option value="' + result[i].id + '">' + result[i].name + '</option>';
					}
					$("#id_select").html(str);
					$('.selectpicker').selectpicker({
						'selectedText': 'cat'
					});
				}
			}
		});
	}

	$(".nav-bar-left").click(function () {
		history.back();
	});
	$("#lg-sub-s").click(function () {
		var name = $("#name").val();
		var phone = $("#phone").val();
		var standbyPhone = $("#standbyPhone").val();
		var word = $("#words").val();
		var loupan = $("#loupan").val();
		var goufang = $("#goufang").val();
		var guwen = $("#id_select").val();
		var memberIds = '';
		for (var i = 0; i < guwen.length; i++) {
			memberIds += guwen[i] + ",";
		}
		memberIds = memberIds.substring(0, memberIds.length - 1);
		var anotherAdviser = "";
		$("input:checkbox:checked").each(function (inx, ele) {
			anotherAdviser += $(ele).val() + ",";
		});

		function isPhoneNo(phone) {
			var pattern = /^1\d{10}$/ //正则表达式
			// var pattern = /^1[345789]\d{9}$/;
			return pattern.test(phone);
		}
		if (name == "") {
			alert("姓名不能为空");
			return false;
		}
		if (phone == "") {
			alert("电话不能为空");
			return false;
		}
		if (isPhoneNo($.trim(phone)) == false) {
			alert("手机号码不正确");
			return false;
		}

		if (loupan == "") {
			alert("请选择意向楼盘");
			return false;
		}
		if (goufang == "") {
			alert("请选择购房意向");
			return false;
		}
		if (guwen == "") {
			alert("请选择置业顾问");
			return false;
		}
		$.ajax({
			type: "post",
			url: BASE_URL + "/staff/report",
			data: {
				"realName": name,
				"phone": phone,
				"sparePhone": standbyPhone,
				"buildingInfoId": loupan,
				"remark": word,
				"demand": goufang,
				"memberIds": memberIds,
			},
			dataType: "json",
			xhrFields: {
				withCredentials: true
			},
			success: function (data) {
				if (data.code == 200) {
					alert(data.msg);
					history.go(-1);
				} else {
					alert(data.msg);
				}
			}
		});
	})
});