$(function() {
	var id = getUrlParam("id");
	var phoneN="";	
	$.ajax({
		type: "get",
		url: BASE_URL + "/voucherInfo/voucherUse",
		data: {
			"content": id

		},
		xhrFields: {
			withCredentials: true
		},
		success: function(data) {
			console.log(data);
			if(data.code != 200) {
				var msg = data.msg;
				alert(msg);
				history.back();
			} else {
				var html = "";
				var ele = data.data;
				phoneN = ele.loginId;
				html += [
					//客户姓名信息
					(function() {
						return '<div class="custom-item" id="">' +
							'<span class="custom-name" id="' + ele.id + '">姓名:</span>' +
							'<input type="text" name="name" id="name" value="' + (ele.name == null ? "" : ele.name) + '" readonly="readonly"/>' +
							'</div>'
					})(),
					(function() {
						if(phoneN != "00000000000") {
							//客户电话
//							(function() {
								return '<div class="custom-item" >' +
									'<span id="">电话:</span>' +
									'<input type="text" name="phone" id="phone" value="' + ele.loginId + '" readonly="readonly"/>' +
									'</div>'+
									'<div class="custom-item custom-phone" id="">' +
									'<span id="" class="code-span">验证码:</span>' +
									'<input type="text" name="phone-code" id="phone-code" value=""/>' +
									'<button id="lg-phone-code-cd" class="icon-info">获取验证码</button>' +
									'</div>'
//							})(),
						}
					})(),

					//客户身份证号
					(function() {
						return '<div class="custom-item" >' +
							'<span id="">身份证号:</span>' +
							'<input type="text" name="IDcode" id="IDcode" value="' + (replaceNull(ele.identityCard) == null ? "" : replaceNull(ele.identityCard)) + '" readonly="readonly"/>' +
							'</div>'
					})(),
					//客户兑换商品信息
					(function() {
						return '<div class="custom-item" id="">' +
							'<span id="">商品名称:</span>' +
							'<input type="text" name="intent" id="intent" value="' + ele.productName + '" readonly="readonly" />' +
							'</div>'
					})(),

					(function() {
						return '<div class="custom-item" id="">' +
							'<span id="">商品型号:</span>' +
							'<textarea rows="2" name="inputTime" id="inputTime" readonly="readonly" >'+ele.productModel+'</textarea>' +
							'</div>'
					})(),
					//商品价格
					(function() {
						return '<div class="custom-item" id="">' +
							'<span id="">商品价格:</span>' +
							'<input type="text" name="demand" id="demand" value="' + ele.productSalePrice + '" readonly="readonly" />' +
							'</div>'
					})(),
					//优惠券金额
					(function() {
						return '<div class="custom-item" id="">' +
							'<span id="">优惠金额:</span>' +
							'<input type="text" name="state" id="state" value="' + ele.preferential + '" readonly="readonly" />' +
							'</div>'
					})(),
					//客户实付金额
					(function() {
						return '<div class="custom-item" id="">' +
							'<span id="">应付金额:</span>' +
							'<input type="text" name="state" id="state" value="' + ele.productPayPrice + '" readonly="readonly" />' +
							'</div>'
					})(),
					//商品供货商
					(function() {
						return '<div class="custom-item" id="">' +
							'<span id="">供货商:</span>' +
							'<input type="text" name="state" id="state" value="' + ele.productDealer + '" readonly="readonly" />' +
							'</div>'
					})(),
				].join('')
				$(".custom-subinfo").html(html);
			}
		}
	});
	var returnCode = "";
	var InterValObj; //timer变量，控制时间 
	var count = 60; //间隔函数，1秒执行 
	var curCount; //当前剩余秒数 
	$(".custom-subinfo").on("click", ".icon-info", function() {
	    phoneN = $("#phone").val();
		$.ajax({
			url: BASE_URL + "/login/getCode",
			data: {
				"loginId": phoneN,
				"type": 2

			},
			xhrFields: {
				withCredentials: true
			},
			type: "get",
			dataType: "json",
			success: function(data) {
				if(data.code != 200) {
					alert(data.msg);

				} else {
					alert("验证码发送成功");
					returnCode = data.data;
					sendMessage();
				}
			}
		});
	});
	$("#submit").click(function() {
		$("#submit").attr("disabled","disabled");
		$("#submit").addClass("btn-no");
		phoneCode = $("#phone-code").val();
		var customId = $(".custom-name").attr("id");
		if(phoneN!='00000000000'){
			if(returnCode != phoneCode) {
				$("#submit").removeAttr("disabled");
				$("#submit").removeClass("btn-no");
				alert("请输入正确的验证码");
				return false;
			}
		}
		
		$.ajax({
			type: "post",
			url: BASE_URL + "/voucherInfo/voucherCheckPass",
			data: {
				"id": customId
			},
			dataType: "json",
			xhrFields: {
				withCredentials: true
			},
			success: function(data) {
				if(data.code == 200) {
					alert(data.msg);
				} else {
					$("#submit").removeAttr("disabled");
					$("#submit").removeClass("add-no");
					alert(data.msg);
					
				}
			},
		});
	});
	
	$(".xiaoyuhao").click(function() {
		history.back();
	});

	function sendMessage() {　
		curCount = count;　　 //设置button效果，开始计时 
		$(".icon-info").attr("disabled", "disabled");
		$(".icon-info").addClass("btnactive");
		$(".icon-info").text(curCount + "秒后可重新发送");
		InterValObj = window.setInterval(SetRemainTime, 1000); //启动计时器，1秒执行一次 
		　　
		//请求后台发送验证码 TODO 

	}

	//timer处理函数 
	function SetRemainTime() {
		if(curCount == 0) {
			window.clearInterval(InterValObj); //停止计时器 
			$(".icon-info").removeAttr("disabled"); //启用按钮 
			$(".icon-info").removeClass("btnactive");
			$(".icon-info").text("重新发送验证码");
		} else {
			curCount--;
			$(".icon-info").text(curCount + "秒后可重新发送");
		}
	}
})