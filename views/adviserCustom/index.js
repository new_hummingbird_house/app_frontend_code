$(function() {
	var dic = [];
	getStateDic(function(res) {
		dic = res;
		getList();
	})
	//初始化函数
	function getList() {
		$.ajax({
			type: "get",
			url: BASE_URL + "/employee/follow",
			data: {
				
			},
			xhrFields: {
				withCredentials: true
			},
			success: function(data) {
				
				var html = "";
				var ele = data.data;
				for(var i = 0; i < ele.length; i++) {
//					console.log(ele[i].progressStatus);
					html += [
						//客户姓名信息
						(function() {
							return '<div class="adviser-custom" id="' + ele[i].id + '">' +
								'<div class="custom-item" id="' + ele[i].sourceType + '">' +
								'<span id="">姓名:</span>' +
								'<input type="text" name="name" id="name" value="' + ele[i].realName + '" readonly="readonly"/>' +
								'</div>'
						})(),
						//客户电话
						(function() {
							return '<div class="custom-item" id="">' +
								'<span id="">电话:</span>' +
								'<input type="text" name="phone" id="phone" value="' + ele[i].phone + '" readonly="readonly"/>' +
								'<span id="" class="icon-info">' +
								'<a href="sms:' + ele[i].phone + '"><i class="iconfont icon-xinfeng1"></i></a>' +
								'</span>' +
								'<span id="" class="icon-info">' +
								'<a href="tel://' + ele[i].phone + '"><i class="iconfont icon-dianhua"></i></a>' +
								'</span>' +
								'</div>'
						})(),

						//状态
						(function() {
							return '<div class="custom-item blanking" id="">' +
								'<span id="">状态:</span>' +
								'<div class="state">' +
								getState(ele[i].progressStatus) +
								'</div>' +
								'</div>' +
								'</div>'
						})(),
					].join('')
				}
				$(".custom-subinfo").html(html);

			}
		});
	}

	//加载字典
	function getStateDic(callback) {
		$.ajax({
			type: "get",
			url: BASE_URL + "/coreDict/getDictByType",
			data: {
				"dictName": "progress_status"
			},
			xhrFields: {
				withCredentials: true
			},
			success: function(data) {
				callback(data.data);
			}
		});

	}

	//获取客户状态
	function getState(state) {
		var str = "",
			flag = false,
			last = false,
			active = ' state-list-act';
		if(state === 0) last = true;
		for(var j = 0; j < dic.length; j++) {
			var ele = dic[j];
			var calc = Number(ele.value) === state;
			str += '<div class="state-list' +
				(function() {
					if(calc && last) return active;
					else if(last) return '';
					else return flag ? '' : active;
				})() +
				'">' +
				'<i class="iconfont icon-yuanquan1"></i>' +
				'<span>' + ele.name + '</span>' +
				'</div>'

			if(calc) flag = true;
		}
		return str;
	}

	$(".xiaoyuhao").click(function() {
		history.back();
	});
	$(".custom-subinfo").on("click", ".adviser-custom", function() {
		var customId = escape($(this).attr("id"));
		var sourceType = escape($(this).children().eq(0).attr("id"));
		var url = "../alterInfo/index.html?id=" + customId + "&sourceType=" + sourceType;
		window.location.href = url;
	})
})