$(document).ready(function () {

	//初始加载数据信息
	$.ajax({
		type: "get",
		url: BASE_URL + "/wode/customer",
		async: true,
		data: {

		},
		xhrFields: {
			withCredentials: true
		},
		success: function (data) {

			if (data.code == 200) {
				$("#petName").val(data.data.customer.nickName);
				$("#name").val(data.data.customer.realName);
				$("#birthday").val(data.data.customer.birthday);
				if (data.data.customer.sex == 1) {
					$('input[name="choseSex"]').eq(0).attr("checked", "true");
				} else if (data.data.customer.sex == 2) {
					$('input[name="choseSex"]').eq(1).attr("checked", "true");
				} else {
					$('input[name="choseSex"]').removeAttr("checked");
				}
				$(".lg-show-img").attr("src", IMG_URL + data.data.customer.photoUrl);
				
				
				if (data.data.customer.photoUrl != null) {
					$(".lg-show-img").removeClass("lg-hide");
					$(".icon-wode").addClass("lg-hide");
				}
				if (data.data.roles.indexOf("zuoXiao") != -1 || data.data.roles.indexOf("xingXiao") != -1) {
					$(".item-other").removeClass("lg-hide");
				}

				$("#revise-pw").click(function () {
					console.log(data.data.type);
					if (data.data.type == "0") {
						$(".show-pw1").toggleClass("lg-hide");
					} else {
						$(".show-pw1").toggleClass("lg-hide");
						$(".sub-item-pw").toggleClass("lg-hide");
					}
				});

			}
		}
	});

	$.ajax({
		type: "get",
		url: BASE_URL + "/building/buildAndRole",
		async: true,
		data: {

		},
		xhrFields: { 
			withCredentials: true
		},
		success: function (data) {
			if(data.code == 200){		
			var str1 = "";
			var str2 = "";
			var buildingInfos = data.data.buildingInfoList; 
			for (var i = 0; i < buildingInfos.length; i++) {
				str1 += '<option value="' + buildingInfos[i].id + '"' +  
				 (function(){
					 if(data.data.nowBuildingInfoId == buildingInfos[i].id){
						return "selected"; 
					 }
				 })()
				
				+ '>' + buildingInfos[i].buildingTitle +
					'</option>';
			};
			var roles = data.data.roleList; 
			for (var j = 0; j < roles.length; j++) {
				var rolename = "";
				if(roles[j].roleName == "zuoXiao"){
					rolename = "坐销";
				}
				if(roles[j].roleName == "xingXiao"){
					rolename = "行销";
				}			
				str2 += '<option value="' + roles[j].id + '"'+
				(function(){
					if(data.data.nowRoleId == roles[j].id){
						return "selected";
					}
				})()  +
				 '>' + rolename +
					'</option>';
			};
			$("#loupan").append(str1);
			$("#guwen").append(str2);
		 }
		},
	});
	
	
	//保存信息
	$("#lg-sub-s").click(function () {
		//用户信息
		var PetName = $("#petName").val();
		var Name = $("#name").val();
		var Birthday = $("#birthday").val();
		var Sex = $('input[name="choseSex"]:checked').val();
		var userImg = $(".upload").val();
		////修改密码
		var orgPw = $("#org-pw").val();
		var setPw = $("#set-pw").val();
		var confirmPw = $("#confirm-pw").val();
		var buildingInfos = $("#loupan").val();
		var roles = $("#guwen").val();

		if (setPw != confirmPw) {
			alert("两次填写的密码不相同");
			return false;
		}
		if (PetName == "") {
			alert("昵称不能为空");
			return false;
		}
		if (Name == "") {
			alert("姓名不能为空");
			return false;
		}
		if (Birthday == "") {
			alert("生日不能为空");
			return false;
		}
		if (Sex == undefined) {
			alert("请选择性别");
			return false;
		}

		$.ajax({
			type: "put",
			url: BASE_URL + "/wode/customer",
			async: true,
			data: {
				"nickName": PetName,
				"realName": Name,
				"birthday": Birthday,
				"sex": Sex,
				"photoUrl": userImg,
				"oldPassword": orgPw,
				"newPassword": setPw,
				"buildingInfoId": buildingInfos,
				"roleId": roles
			},
			xhrFields: {
				withCredentials: true
			},
			success: function (data) {
				if (data.code == 200) {
					alert("保存成功");
					history.go(-1);
				} else {
					alert(data.msg);
				}
			}
		});
	});
	$(".xiaoyuhao").click(function () {
		history.back();
	});
	$("#proImage").change(function () {
		$(".lg-show-img").removeClass("lg-hide");
		var file = this.files[0];
		if (!/image\/\w+/.test(file.type)) {
			alert("请确保文件为图像类型");
			return false;
		}
		var reader = new FileReader();
		reader.readAsDataURL(file); //调用自带方法进行转换  
		reader.onload = function (e) {
			$(".lg-show-img").attr("src", this.result); //将转换后的编码存入src完成预览  
			$(".upload").val(this.result); //将转换后的编码保存到input供后台使用  
			var img = $(".upload").val();
			var imgNum = img.split(";base64,");
			var imgBase = imgNum[1];
			createCanvas(this.result);
		}

	});

	function createCanvas(src) {
		var img = new Image();
		img.src = src;
		img.onload = function () {
			var expectWidth = img.width;
			var expectHeight = img.height;
			var canvas = document.createElement("canvas");
			var ctx = canvas.getContext("2d");
			var screenWidth = 200;
			canvas.width = screenWidth;
			canvas.height = (expectHeight / expectWidth) * screenWidth;
			ctx.drawImage(this, 0, 0, canvas.width, canvas.height);

			var base64 = canvas.toDataURL("image/jpeg", 0.7);
		}
	}
})
