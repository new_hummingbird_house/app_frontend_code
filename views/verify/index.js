$(function() {
	function notpass(){
		$.ajax({
		type: "get",
		cache:false,
		url: BASE_URL + "/voucherApplication/getVoucherApplicationToCheck",
		data: {
			
		},
		xhrFields: {
			withCredentials: true
		},
		success: function(data) {
			var html = "";
			var ele = data.data;
			for(var i = 0; i < ele.length; i++) {
				html += [
					//客户姓名
					(function() {
						return '<div class="custom-title">'+
						'<div class="custom-item blanking" id="'+ele[i].id+'">' +
							'<span id="">姓名:</span>' +
							'<input type="text" name="name" id="name" value="'+ele[i].name+'" readonly="readonly"/>' +
							'</div>'
					})(),
					
					//客户购买楼盘
					(function() {
						return '<div class="custom-item" id="">' +
							'<span id="">楼盘信息:</span>' +
							'<input type="text" name="intent" id="intent" value="'+ele[i].buildingTitle+'" readonly="readonly" />' +
							'</div>'
					})(),
					//信息提交时间
					(function() {
						return '<div class="custom-item" id="">' +
							'<span id="">提交时间:</span>' +
							'<input type="text" name="inputTime" id="inputTime" value="'+ele[i].submissionTime+'" readonly="readonly" />' +
							'</div>'+
							'</div>'
					})(),
					
					
				].join('')
			}
			$(".custom-subinfo").html(html);
			
		}
	});
	}
	
	function pass(){
		$.ajax({
		type: "get",
		url: BASE_URL + "/voucherApplication/getVoucherApplicationChecked",
		data: {
			
		},
		xhrFields: {
			withCredentials: true
		},
		success: function(data) {
			var html = "";
			var ele = data.data;
			for(var j = 0; j < ele.length; j++) {
				html += [
					//客户姓名
					(function() {
						return '<div class="custom-title">'+
						'<div class="custom-item blanking" id="'+ele[j].id+'">' +
							'<span id="">姓名:</span>' +
							'<input type="text" name="name" id="name" value="'+ele[j].name+'" readonly="readonly"/>' +
							'</div>'
					})(),
					
					//客户购买楼盘
					(function() {
						return '<div class="custom-item" id="">' +
							'<span id="">楼盘信息:</span>' +
							'<input type="text" name="intent" id="intent" value="'+ele[j].buildingTitle+'" readonly="readonly" />' +
							'</div>'
					})(),
					//信息提交时间
					(function() {
						return '<div class="custom-item" id="">' +
							'<span id="">审核时间:</span>' +
							'<input type="text" name="inputTime" id="inputTime" value="'+ele[j].checkTime+'" readonly="readonly" />' +
							'</div>'+
							'</div>'
					})(),
					
					
				].join('')
			}
			$(".custom-subinfo").html(html);
			
		}
	});
	}
	notpass();
	$("#notPass").addClass("btn-act");
	$(".xiaoyuhao").click(function() {
		history.back();
	});
	$("#notPass").click(function(){
		notpass();
		$("#pass").removeClass("btn-act");
		$(this).addClass("btn-act");
	});
	$("#pass").click(function(){
		pass();
		$("#notPass").removeClass("btn-act");
		$(this).addClass("btn-act");
	});
	$(".custom-subinfo").on("click",".custom-title",function(){
		var customId = escape($(this).children(".custom-item").attr("id"));
        var url = "../passInfo/index.html?id=" + customId;
        window.location.href = url;
	});
})