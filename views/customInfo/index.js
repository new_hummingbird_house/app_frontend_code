$(function() {
	$.ajax({
		type: "get",
		url: BASE_URL + "/userDemand/findUserDemandDict",
		data: {

		},
		xhrFields: {
			withCredentials: true
		},
		success: function(data) {
			var html = "";
			var ele = data.data;
			html += [
				//户型面积
				(function() {
					return '<div class="custom-item" id="">' +
						'<span id="">面积区间:</span>' +
						'<input type="text" class="range" name="minArea" id="min-area" value="" />—' +
						'<input type="text" class="range" name="maxArea" id="max-area" value="" />' +
						'</div>'
				})(),

				//希望所在区
				(function() {
					return '<div class="custom-item" id="">' +
						'<span id="">期望所在区:</span>' +
						'<div class="build-area">' +

						'</div>' +
						'</div>'
				})(),
				//价位区间
				(function() {
					return '<div class="custom-item" id="">' +
						'<span id="">价格区间:</span>' +
						'<input type="text" class="range" name="minPrice" id="min-price" value="" />—' +
						'<input type="text" class="range" name="maxPrice" id="max-price" value="" />' +
						'</div>'
				})(),
				//房屋具备特色
				(function() {
					return '<div class="custom-item" id="">' +
						'<span id="">房屋特色:</span>' +
						'<div id="buildPoint">' +
						'</div>' +
						'</div>'
				})(),
				//户型要求
				(function() {
					return '<div class="custom-item" id="">' +
						'<span id="">户型要求</span>' +
						'<button class=" btn addBtn" id="add">添加</button>' +
						'</div>'
				})(),
				(function() {
					return '<div class="custom-item addInfo" id="">' +
						'<div class="buyinfo">' +
						'<input type="text" name="room" id="2" value="">室' +
						'<input type="text" name="hall" id="3" value="">厅' +
						'<input type="text" name="hutch" id="4" value="">卫' +
						'</div>' +
						'</div>'
				})(),
//				(function() {
//					return '<div class="custom-item addInfo">' +
//
//						
//				})(),
			].join('')
			$(".custom-subinfo").html(html);
			var ele = "";
			for(var j = 0; j < data.data.buildingArea.length; j++) {
				var str = data.data.buildingArea[j];

				ele += '<input type="checkbox" data-value="'+str.name+'" name="buildArea" id="' + str.id + '" value="' + str.id + '" />' + str.name
			}
			$(".build-area").html(ele);

			var list = "";
			for(var i = 0; i < data.data.advantage.length; i++) {
				var str = data.data.advantage[i];
				list += '<a class="list-subtitle" data-text="' + str.name + '" data-value="'+str.value+'"  id="' + str.id + '">' + str.name + '</a>';
			}
			$("#buildPoint").html(list);
			demandDetail();
		}
	});
	$("#submit").click(function() {
		var minArea = $("#min-area").val();
		var maxArea = $("#max-area").val();
		var minPrice = $("#min-price").val();
		var maxPrice = $("#max-price").val();
		var buildingArea = '';
		var advantage = '';
		var apartment = '';
		var id = $("#demandId").val();
		$("input:checkbox:checked").each(function(inx,ele){
			buildingArea+=$(ele).val()+",";
		});
		buildingArea = buildingArea.substring(0,buildingArea.length-1);
		$(".actbtns").each(function(inx,ele){
			advantage += $(ele).attr("data-value")+",";
		});
		advantage = advantage.substring(0,advantage.length-1);
		$(".buyinfo").each(function(inx,ele){
			var room = $(ele).children("[name='room']").val();
			var hall = $(ele).children("[name='hall']").val();
			var hutch = $(ele).children("[name='hutch']").val();
			if(room==''&&hall==''&&hutch==''){
				return true;
			}
			if(room=='') room = 0;
			if(hall=='') hall = 0;
			if(hutch=='') hutch = 0;
			apartment += room+"-"+hall+"-"+hutch+",";
		});
		apartment = apartment.substring(0,apartment.length-1);
		$.ajax({
			type: "post",
			url: BASE_URL + "/userDemand/saveOrUpdateDemand",
			data: {
				"minArea":minArea,
				"maxArea":maxArea,
				"minPrice":minPrice,
				"maxPrice":maxPrice,
				"buildingArea":buildingArea,
				"advantage":advantage,
				"apartment":apartment,
				"id":id
			},
			dataType: "json",
			xhrFields: {
				withCredentials: true
			},
			success: function(data) {
				if(data.code == 200) {
					alert("成功");
					history.back();
				}else{
					alert(data.msg);
				}
			}
		});
	});
	$(".custom-subinfo").on("click", "#add", function() {
		var add = "";
		add += '<div class="custom-item addInfo">' +
		'<div class="buyinfo">' +
		'<input type="text" name="room" id="" value="">室' +
			'<input type="text" name="hall" id="" value="">厅' +
			'<input type="text" name="hutch" id="" value="">卫'+
			'<button class=" btn addBtn" id="delete">删除</button>' +
			'</div>' +
			'</div>'
		$(".custom-subinfo").append(add);
	});
	$(".custom-subinfo").on("click", ".list-subtitle", function() {
		$(this).toggleClass("actbtns");
	});
	$(".custom-subinfo").on("click", "#delete", function() {
		$(this).parent(".buyinfo").remove();
	});
	$(".xiaoyuhao").click(function() {
		history.back();
	});
	
	function demandDetail(){
		$.ajax({
			type:"get",
			url:BASE_URL+"/userDemand/findUserDemand",
			async:true,
			dataType: "json",
			xhrFields: {
				withCredentials: true
			},
			success: function(data) {
				if(data.code == 200) {
					var minArea = data.data.minArea;
					var maxArea = data.data.maxArea;
					var minPrice = data.data.minPrice;
					var maxPrice = data.data.maxPrice;
					var buildingAreaList = data.data.buildingAreaList;
					var advantageList = data.data.advantageList;
					var apartmentList = data.data.apartmentList;
					var id = data.data.id;
					$("#demandId").val(id);
					$("#min-area").val(minArea);
					$("#max-area").val(maxArea);
					$("#min-price").val(minPrice);
					$("#max-price").val(maxPrice);
					$(buildingAreaList).each(function(inx,ele){
						$("[data-value="+ele+"]").attr("checked","true");
					});
					$(advantageList).each(function(inx,ele){
						$("[data-text="+ele+"]").addClass("actbtns");
					});
					var apartLen = $(apartmentList).length;
					$(apartmentList).each(function(inx,ele){
						var room = ele.room=='0'?'':ele.room;
						var hall = ele.hall=='0'?'':ele.hall;
						var hutch = ele.hutch=='0'?'':ele.hutch;
						$(".buyinfo").eq(inx).children("[name='room']").val(room);
						$(".buyinfo").eq(inx).children("[name='hall']").val(hall);
						$(".buyinfo").eq(inx).children("[name='hutch']").val(hutch);
						if(apartLen==inx+1) return false;
						$("#add").click();
					});
				}
			}
		});
	}
})