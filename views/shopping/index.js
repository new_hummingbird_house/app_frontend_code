$(function() {
	window.onresize = function() {
		var windowHeight = $(window).height(); //获取窗口的可见高度,不是整个文档的高度
		var screenHeight = window.screen.availHeight; //获取浏览器的屏幕的可用高度
		var u = navigator.userAgent;

		if(u.indexOf('Android') > -1 || u.indexOf('Linux') > -1) {
			if(windowHeight <= screenHeight / 2) {
				$('.footer').css({
					'position': 'absoult',
					'display': 'none'
				});
			} else {
				$('.footer').css({
					'position': 'fixed',
					'display': 'block'
				});
			}
		}
	}
	$("input[type='text']").on('focus', function() {
		$('.footer').hide();
	})
	$("input[type='text']").on('blur', function() {
		$('.footer').show();
	})

	var typeVal = 1,
		searchVal = "";

	function getPic() {
		$.ajax({
			type: "GET",
			url: BASE_URL + "/advert/advertPic",
			async: true,
			data: {
				type: 2
			},
			xhrFields: {
				withCredentials: true
			},
			success: function(data) {
				var photoStr = "";
				if(data != null) {
					var data = data.data;
					for(var i = 0; i < data.length; i++) {
						photoStr = photoStr + "<div class=\"swiper-slide\">" +
							" <img src=\"" + IMG_URL + data[i].picPath + "\"></div>"
					}
					$(".swiper-wrapper").html(photoStr);
				}
				$(".swiper-wrapper").html(photoStr);

				var mySwiper = new Swiper('.swiper-container', {
					autoplay: {
						delay: 3000,
						stopOnLastSlide: false,
						disableOnInteraction: false
					},
					//					 observer:true,
					// 如果需要分页器
					pagination: {
						el: '.swiper-pagination',
					},
				});
			}
		});
	}

	function productList() {
		var search = $("#search").val();
		$.ajax({
			type: "get",
			url: BASE_URL + "/mall/getMallList",
			data: {
				"keyWord": searchVal || '',
				"type": typeVal || 1,
				"sys": '2'
			},
			async: false,
			dataType: "json",
			xhrFields: {
				withCredentials: true
			},
			success: function(data) {
				var html = "";
				var photoStr = "";
				var data = data.data;
				for(var i = 0; i < data.mallInfoList.length; i++) {
					//赋值对象
					var ele = data.mallInfoList[i];
					html += [
						//最外层框子
						'<div class="goods-sublist" id="' + ele.id + '">',
						//图片信息
						(function() {
							return '<img src="' + IMG_URL + ele.productPic + '"/>'
						})(),
						//商品信息
						(function() {
							return '<div class="goods-con">' +
								'<p class="goods-text" id="goods-text-' + ele.id + '">' + ele.productName + ele.productModel + '</p>' +
								'<p class="code-price"><span class="price">￥</span>' + parseFloat(ele.discountPrice) + '<span class="fnlogo">蜂鸟特惠</span></p>' +
								'<span class="sale-price">价格：￥' + parseFloat(ele.productSalePrice) + '</span>' +
								'<span class="buy-text">首次购买用券立享优惠</span>' +
								'</div>'
						})(),
						'</div>'
					].join('')
				}
				$(".goods").html(html);
			}
		});
		
		$.ajax({
			type: "get",
			url: BASE_URL + "/browsingHistory/check",
			data: {
				id : "1"
			},
			async: false,
			dataType: "json",
			xhrFields: {
				withCredentials: true
			},
			success: function(data) {
				console.log(data);
				var browsing = data.data;
				for(var index = 0, len = browsing.length; index < len; index++){
					var browsingId = browsing[index].browsingId;
					var p_id = "#goods-text-" + browsingId;
					$(p_id).addClass("add-color");
				}
			}
		});		
	}
	productList();
	getPic();
	$("#icon-search").click(function() {
		searchVal = $('#search').val();
		productList();
	})
	$(".goods-list").children("span").click(function() {
		//		search.val('');
		//		searchVal = "";
		typeVal = $(this).attr("id");
		productList();
	});

	$(".goods").on("click", ".goods-sublist", function() {
		$(this).css("color","#aaa");
		var goodsId = escape($(this).attr("id"));
		var url = "../product/index.html?id=" + goodsId;
		window.location.href = url;
	});
// 	$(".nav-bar-left").click(function() {
// 		history.back();
// 	});
	$(".nav-bar-left").click(function () {
			var url = "../index/index.html";
			window.location.href = url;
	});
	$(".icon-right-home").click(function() {
		window.location.href = "../index/index.html";
	});
});