$(function(){
	var id = getUrlParam("id");
	$.ajax({
		type:"get",
		url:BASE_URL+"/zixun/details",
		data:{
			id:id
		},
		async:true,
		xhrFields: {
			withCredentials: true
		},
		success:function(data){
			$(".title").html(data.data.title)
			$(".maintext").html(data.data.content);
			$(".subtitle font").eq(1).html(data.data.createDate);
		}
	});

	$(".xiaoyuhao").click(function() {
		history.back();
	});
})
