$(document).ready(function () {

	$(".lg-normal-tab").click(function () {
		var index = $(this).index();
		var status = $(this).attr("data-state");
		voucher(status);
		$(".lg-normal-tab").removeClass("lg-tab-active");
		$(this).addClass("lg-tab-active");
		if ($(".lg-normal-tab").hasClass("lg-tab-active")) {
			$(".lg-normal-f").addClass("lg-hide");
			$(".lg-normal-f").eq(index).removeClass("lg-hide");
		}
	});
});

function voucher(status) {
	var status = status;
	$.ajax({
		url: BASE_URL + "/voucherInfo/voucherQuery",
		data: {
			"status": status
		},
		type: "get",
		dataType: "json",
		xhrFields: {
			withCredentials: true
		},
		success: function (data) {
			console.log(data);
			var str = "";
			for (var i = 0; i < data.data.length; i++) {
				var result = data.data[i];
				var type = result.type;

				str += '<div class="items" id="' + result.id + '">' +
					(function(){
					   if(status=='0'){
						return '<img class="yh-img" src="../../img/yh.png">';
					  }else if(status=='1'||status=='2'){
						return '<img class="yh-img" src="../../img/yh-2.png">';  
					  }
					  })() +
					'<div class="datashow ">' +
					'<p class="list-title "> 使用范围：</p>' +
					'<div class="adress">' +
					   (function () {if (type == '0') {
						   return '<span>用于兑换' + result.productName + '商品</span>';
					     }
						 else if (type == '1') {
							return '<span>' + result.productName + '商品' + '</span>' +
							'<span>' + result.preferential + '代金券</span>';
						}})() + 
					'</div>' +
					'<p class="subtitle">有效期：</p>' +
					'<p class="detail ">' + result.startTime + '至' + result.dueTime + '</p>' +
					'</div>' +
					(function () {
						if (status == '1') {
							return '<a class="yh-icon"><i class="iconfont icon-yishiyong"></i></a>'
						}else if (status == '2') {
							return '<a class="yh-icon-data"><i class="iconfont icon-yiguoqi"></i></a>'
						}else{
							return '';
						}
					})() +
					'</div>' +
					'</div>';
			}
			$(".lg-normal-f").html(str);

		}
	});
}

$(function () {
	voucher(0);

	//点击跳转

	$(".nav-bar-left").click(function () {
		history.back();
	});
	$("#yh-1").on("click", ".items", function () {
		var id = escape($(this).attr("id"));
		var url = "../yhcode/index.html?id=" + id;
		window.location.href = url;
	});
});
