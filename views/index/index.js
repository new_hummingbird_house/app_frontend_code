$(function() {
	window.onresize = function() {
		var windowHeight = $(window).height(); //获取窗口的可见高度,不是整个文档的高度
		var screenHeight = window.screen.availHeight; //获取浏览器的屏幕的可用高度

		var u = navigator.userAgent;

		if(u.indexOf('Android') > -1 || u.indexOf('Linux') > -1) {
			if(windowHeight <= screenHeight / 2) {
				$('.footer').css({
					'position': 'absoult',
					'display': 'none'
				});
			} else {
				$('.footer').css({
					'position': 'fixed',
					'display': 'block'
				});
			}
		}
	}
	$("input[type='text']").on('focus', function() {
		$('.footer').hide();
	})
	$("input[type='text']").on('blur', function() {
		$('.footer').show();
	})

	function pagelist() {
		$.ajax({
			type: "GET",
			url: BASE_URL + "/advert/advertPic",
			async: true,
			data: {
				type: 1
			},
			xhrFields: {
				withCredentials: true
			},
			success: function(data) {
				var photoStr = "";
				if(data != null) {
					var data = data.data;
					for(var i = 0; i < data.length; i++) {
						photoStr = photoStr + "<div class=\"swiper-slide\">" +
							" <img src=\"" + IMG_URL + data[i].picPath + "\"></div>"
					}
					$(".swiper-wrapper").html(photoStr);
				}
				/*轮播js放在此处*/
				var mySwiper = new Swiper('.swiper-container', {
					autoplay: {
						delay: 3000,
						stopOnLastSlide: false,
						disableOnInteraction: false
					},
					// 如果需要分页器
					pagination: {
						el: '.swiper-pagination',
					},
				});
			}
		});

		$.ajax({
			type: "GET",
			url: BASE_URL + "/index.json",
			async: true,
			data: {
				// 				"type": type,
				// 				"sys": '2'
			},
			xhrFields: {
				withCredentials: true
			},
			success: function(data) {
				var html = "";
				var list = "";
				for(var i = 0; i < data.data.length; i++) {
					var result = data.data[i];
					var buildId = result.id;
					html += '<div class="list-style" id="' + buildId + '">' +
						'<div class="items">' +
						'<img class="list-img" src="../../img/loading.gif" data-src="' + IMG_URL + result.buildingImage + '">' +
						'<div class="datashow">' +
						'<p class="list-title">' + result.buildingTitle + '</p>' +
						'<p class="adress"><span>' + result.areaName + '-' + result.areaName +
						'</span> <span class="money">' +
						 (function(){
						 	if(result.buildingPrice==undefined){
						 		return "价格待定";
						 	}else{
						 		return  result.buildingPrice +'元/m<sup>2</sup></span></p>' ;
						 	}
						 })()+
						'<p class="detail">建面:' +
						(function(){
							if(result.minArea==null||result.maxArea==null){
								return "暂无数据";
							}else{
								return result.minArea + '-' + result.maxArea+"m<sup>2</sup>";
							}
						})()+
						'</p>' +
						'<p class="press">' +
						'<a class="list-subtitle list-subtitle-zz">' +
						(function(){
							if(result.propertyType==null){
								return "";
							}else{
								return result.propertyType;
							}
						})()+
					   '</a>' +
						'<a class="list-subtitle list-subtitle-zs">' + result.buildingState +
						'</a>' +
						'</p>' +
						'</div>' +
						'</div>' +
						'</div>' +
						'</div>'
				}
				$(".show-list").html(html);
				for(var i = 0; i < data.data.length; i++) {
					var result = data.data[i];
					var list = "";
					for(var j = 0; j < result.buildingAdvantageList.length; j++) {
						var str = result.buildingAdvantageList[j];
						list += '<a class="list-subtitle">' + str + '</a>';
					}
					$(".press").eq(i).append(list);
				}	
						lazyload();
			},		
		});
	}
	pagelist();

	function lazyload(){
			// 获取window的引用:
		var $window = $(window);
		// 获取包含data-src属性的img，并以jQuery对象存入数组:
		var lazyImgs = _.map($('img[data-src]').get(), function (i) {
				return $(i);
		});
		// 定义事件函数:
		var onScroll = function() {
				// 获取页面滚动的高度:
				var wtop = $window.scrollTop();
				// 判断是否还有未加载的img:
				if (lazyImgs.length > 0) {
						// 获取可视区域高度:
						var wheight = $window.height();
						// 存放待删除的索引:
						var loadedIndex = [];
						// 循环处理数组的每个img元素:
						_.each(lazyImgs, function ($i, index) {
								// 判断是否在可视范围内:
								if ($i.offset().top - wtop < wheight) {
										// 设置src属性:
										$i.attr('src', $i.attr('data-src'));
										// 添加到待删除数组:
										loadedIndex.unshift(index);
								}
						});
						// 删除已处理的对象:
						_.each(loadedIndex, function (index) {
								lazyImgs.splice(index, 1);
						});
				}
		};
		// 绑定事件:
		$window.scroll(onScroll);
		// 手动触发一次:
		onScroll();	
	}
	
	
	$(".nav-bar-right").click(function() {
		window.location.href = "../about/index.html";
	});
	//搜索查询
	$("#icon-search").click(function() {
		var navSearch = escape($("#search").val());
		var url = "../new/index.html?keyWord=" + navSearch;
		window.location.href = url;
	});
	$(".list-box").on("click", ".list-style", function() {
		var buildId = escape($(this).attr("id"));
		var url = "../detail/index.html?id=" + buildId;
		window.location.href = url;
	});
	$(".footer-nav").click(function() {
		$(".footer-nav").removeClass("active");
		$(this).addClass("active");
	});
});

