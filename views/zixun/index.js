$(function() {
	$.ajax({
		type:"get",
		url:BASE_URL+"/zixun/index",
		async:true,
		crossDomain: true,
		xhrFields: {
			withCredentials: true
		},
		data:{
			
		},
		success: function(data) {
				var html = "";
				for(i = 0; i < data.data.length; i++) {
					var result = data.data[i];
					html += '<div class="list" id='+result.id+'>' +
						'<div class="items">' +
						'<img class="list-img" src="' + IMG_URL+ result.imageUrl + '">' +
						'<div class="datashow">' +
						'<p class="list-title">' + result.title + '</p>' +
						'<span class="detail">'+
                '<i class="iconfont icon-CombinedShape-"></i>'+result.readNum+'</span>'+
            '<span class="subdetail">'+
								'<i class="iconfont icon-shijianzhongbiao2"></i>'+timeago(result.createDate)+'</span>'+
						'</div>' +
						'</div>' +
						'<div class="press">' +
						'<a class="list-subtitle" href="">热点</a>' +
						'</div>' +
						'</div>'
				}
				$(".content").html(html);
			}
	});
	$(".content").on("click",".list",function() {
		var zixunId = escape($(this).attr("id"));
		var url = "../zixun-2/index.html?id=" + zixunId;
		window.location.href = url;
	});
	$(".fanhui").click(function () {
        history.back();
   });
	function timeago(dateTime){   //dateTimeStamp是一个时间毫秒，注意时间戳是秒的形式，在这个毫秒的基础上除以1000，就是十位数的时间戳。13位数的都是时间毫秒。
		dateTime = dateTime.substring(0,19);    
		dateTime = dateTime.replace(/-/g,'/'); 
		var dateTimeStamp = new Date(dateTime).getTime();
		var minute = 1000 * 60;      //把分，时，天，周，半个月，一个月用毫秒表示
		var hour = minute * 60;
		var day = hour * 24;
		var week = day * 7;
		var halfamonth = day * 15;
		var month = day * 30;
		var now = new Date().getTime();   //获取当前时间毫秒
		console.log(now)
		var diffValue = now - dateTimeStamp;//时间差
		
		if(diffValue < 0){
		    return;
		}
		var minC = diffValue/minute;  //计算时间差的分，时，天，周，月
		var hourC = diffValue/hour;
		var dayC = diffValue/day;
		var weekC = diffValue/week;
		var monthC = diffValue/month;
		if(monthC >= 1 && monthC <= 3){
		    result = " " + parseInt(monthC) + "月前"
		}else if(weekC >= 1 && weekC <= 3){
		    result = " " + parseInt(weekC) + "周前"
		}else if(dayC >= 1 && dayC <= 6){
		    result = " " + parseInt(dayC) + "天前"
		}else if(hourC >= 1 && hourC <= 23){
		    result = " " + parseInt(hourC) + "小时前"
		}else if(minC >= 1 && minC <= 59){
		    result =" " + parseInt(minC) + "分钟前"
		}else if(diffValue >= 0 && diffValue <= minute){
		    result = "刚刚"
		}else {
		    var datetime = new Date();
		    datetime.setTime(dateTimeStamp);
		    var Nyear = datetime.getFullYear();
		    var Nmonth = datetime.getMonth() + 1 < 10 ? "0" + (datetime.getMonth() + 1) : datetime.getMonth() + 1;
		    var Ndate = datetime.getDate() < 10 ? "0" + datetime.getDate() : datetime.getDate();
		    var Nhour = datetime.getHours() < 10 ? "0" + datetime.getHours() : datetime.getHours();
		    var Nminute = datetime.getMinutes() < 10 ? "0" + datetime.getMinutes() : datetime.getMinutes();
		    var Nsecond = datetime.getSeconds() < 10 ? "0" + datetime.getSeconds() : datetime.getSeconds();
		    result = Nyear + "-" + Nmonth + "-" + Ndate
		    }
	    return result;
	}
});
