var BASE_URL = 'https://house.fengniaoai.com/admin';
var IMG_URL = 'https://house.fengniaoai.com/admin/getPic';
var ANON_URL = new Array(
	"/views/index/index.html",
	"/views/login/index.html",
	"/views/about/index.html"
);
var UserRole;
var UserAuth;
$(function() {
	backBtnLis();
	router();
});

function replaceNull(str){
	if(str == 'null'||str == null){
		return '';
	} else{ 
		return str;
	};
}

//返回键监听
function backBtnLis() {
	document.addEventListener('plusready', function() {
		var webview = plus.webview.currentWebview();
		plus.key.addEventListener('backbutton', function() {
			webview.canBack(function(e) {
				if(e.canBack) {
					webview.back();
				} else {
					//webview.close(); //hide,quit
					//plus.runtime.quit();
					mui.plusReady(function() {
						//首页返回键处理
						//处理逻辑：1秒内，连续两次按返回键，则退出应用；
						var first = null;
						plus.key.addEventListener('backbutton', function() {
							//首次按键，提示‘再按一次退出应用’
							if(!first) {
								first = new Date().getTime();
								mui.toast('再按一次退出应用');
								setTimeout(function() {
									first = null;
								}, 1000);
							} else {
								if(new Date().getTime() - first < 1500) {
									plus.runtime.quit();
								}
							}
						}, false);
					});
				}
			})
		});
	});
}

//前端权限路由
function router() {
	$.ajax({
		type: "get",
		url: BASE_URL + "/wode/customer",
		async: true,
		cache: false,
		xhrFields: {
			withCredentials: true
		},
		success: function(data) {
			if(data.code == 200) {
				//授权
				UserRole = data.data.roles;
				UserAuth = data.data.appPermissions;
			} else {
				//跳转到登陆页，不是游客就重定向
				if(!isAnonUrl()) {
					var returnUrl = "returnUrl=" + getRelUrl()+location.search;
					location.replace("../login/index.html?" + returnUrl);
				}
			}
		},
	});
}

//判断是否是游客地址
function isAnonUrl() {
	if(ANON_URL.indexOf(getAbsUrl()) != -1) {
		return true;
	} else {
		return false;
	}
}

//获取当前绝对URL地址
function getAbsUrl() {
	var pathname = window.location.pathname;
	pathname = pathname.substring(pathname.indexOf("views") - 1, pathname.length);
	return pathname;
}

//获取当前相对URL地址
function getRelUrl() {
	var absUrl = getAbsUrl();
	var relUrl = absUrl.replace("/views", "..");
	return relUrl;
}

//获取URL参数
function getUrlParam(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
	var r = window.location.search.substr(1).match(reg); //匹配目标参数
	if(r != null) return unescape(r[2]);
	return null; //返回参数值
}

//扩展Date功能：将long型日期转换为特定的格式
Date.prototype.format = function(format) {
	var o = {
		"M+": this.getMonth() + 1,
		"d+": this.getDate(),
		"h+": this.getHours(),
		"m+": this.getMinutes(),
		"s+": this.getSeconds(),
		"q+": Math.floor((this.getMonth() + 3) / 3),
		"S": this.getMilliseconds()
	}
	if(/(y+)/.test(format)) {
		format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	}
	for(var k in o) {
		if(new RegExp("(" + k + ")").test(format)) {
			format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
		}
	}
	return format;
}

//将long型日期转换为特定格式
function getFormatDate(l, pattern) {
	date = new Date(l);
	if(pattern == undefined) {
		pattern = "yyyy-MM-dd hh:mm:ss";
	}
	return date.format(pattern);
}