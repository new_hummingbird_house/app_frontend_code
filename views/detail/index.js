$(function () {
    var id = getUrlParam("id");
    $(".views").click(function () {
        var url = "../huxing/index.html?id=" + id;
        window.location.href = url;
    });

    $.ajax({
        url: BASE_URL+"/building/details",
        data: {
            "id": id
        },
        xhrFields: {
			withCredentials: true
		},
        type: "get",
        dataType: "json",
        success: function (data) {
            var data = data.data;
            $("#title").html(data.buildingTitle);
            var price = data.buildingPrice;
            if (price == null || price == "") {
                $("#price").html("待定");
            } else {
                $("#price").html(price);
            }
						
            var opDate = data.opDate;
            if (opDate == null || opDate == "") {
                $("#open").html("待定");
            } else {
                $("#open").html(opDate);
            }
            $("#adress").html(data.buildingAddress);

            /*置业顾问*/
            var buildingAdviserList = data.buildingAdviserList;
            var adviserStr = "";
            if (buildingAdviserList != null) {
                for (var i = 0; i < buildingAdviserList.length; i++) {
                    adviserStr = adviserStr +
										     '<p class="consult" id="consult">置业顾问</p>'+
										      '<div id="consultant-box">'+									       
                        '<div class="consultant-info">' +
                        '<div class="consultant-base">' +
                        '<img class = "consultant-img" src="' + IMG_URL+ buildingAdviserList[i].photo +
                        '">' +
                        '<div class="name-wrap">' +
                        '<span class="conname" id="conname">' + buildingAdviserList[i].name +
                        '</span>' +
                        '</div>' +
                        '</div>' +
                        '<a class="ui-circle_phone" href="tel:' + buildingAdviserList[i].phone +
                         '" id="phone">' +
                        '<i class="iconfont icon-dianhua"></i>' +
                        '</a>' +
                        '</div> </div>';
                }
                $("#list-box").html(adviserStr);
            }
						
						/*dianhua*/
						var dianhua = data.buildingPhone;
						var consultStr = "";					
										consultStr += '<div data-tag="1" class="lptel" style="display: -webkit-box;">' +
														'<div class="telinfo">' +
																'<span class="num">' + dianhua +'</span>' +
																'<p class="ui-line-overflow">安全通话隐藏真实号码，免费致电了解更多信息</p>' +
														'</div>' +
														'<a class="phone" href="tel:' + dianhua + '">' +
																'<i class="iconfont icon-dianhua"></i>' +
														'</a>' +
												'</div>';			
								$(".yf-phone").html(consultStr);	
											
						/*dibu*/
						var clickStr = "";			
 										clickStr += '<a class="footer-coupon">' +
 																		'<button class="btn" type="button">获取代金券</button>' +
 																	'</a>' +
																	'<a class="ui-circle_phone" href="tel:' + dianhua + '">' +
 																		'<button class="btn" type="button">拨打电话</button>' +
																	'</a>';	
								 $(".footer").html(clickStr);
					
            /*tupian*/
            var buildingPhotoList = data.buildingPhotoList;
            var photoStr = '<div class="swiper-slide">' +
						                   '<img id="img1" src="' + IMG_URL+ data.buildingImage +
                          '"></div>';
            if (buildingPhotoList != null) {
                for (var i = 0; i < buildingPhotoList.length; i++) {
                    photoStr = photoStr + '<div class="swiper-slide">' +
                        '<img src="'+ IMG_URL+ buildingPhotoList[i].photoUrl + '"></div>';
                }								
            }
        $(".swiper-wrapper").html(photoStr);

            /*huxingtupian*/
            $("#ui-txt_h5").html(data.apartmentPrice);
            $("#ui-tag_orange").html(data.apartmentStateCn);
            $("#img3").attr("src", data.apartmentImg);
            var buildingApartmentList = data.buildingApartmentList;
            var apartmentStr = "";
            var len = buildingApartmentList.length;
            if (len > 3) {
                len = 3
            }
            if (buildingApartmentList != null) {
                for (var i = 0; i < len; i++) {
                    apartmentStr = apartmentStr +
                        '<div class="iscroll">' +
                        '<div class="hx-img-wrap">' +
                        '<img src="' + IMG_URL+ buildingApartmentList[i].apartmentImg + '">' +
                        '</div>' +
                        '<div class="hx-base">' +
                        '<div class="hx-alias">' +
                        buildingApartmentList[i].room + '室' + buildingApartmentList[i].hall + '厅' +
                        buildingApartmentList[i].hutch + '卫 &nbsp;' +
												 (function(){
													 if(buildingApartmentList[i].coveredArea==null){
														 return ""
													 }else{
														 return buildingApartmentList[i].coveredArea  +
                        'm²'
													 }
												 })()
												  +
                        '</div>' +
                        '<div class="ui-txt_h5">' +
												'<span class="ui-txt_h5" id="ui-txt_h5">' + 
												(function(){
													if(buildingApartmentList[i].apartmentPrice==null){
														return ""
													}else{
														return +buildingApartmentList[i].apartmentPrice  +'元/m²</span>' 
													}
												})()
                        +
                        '</div>' +
                        '</div>' +
                        '</div>';
                } 
                $(".hx-wrapper-show").html(apartmentStr);
            }

            /*优势*/
            var buildingAdvantageList = data.buildingAdvantageList;
            var advantage = "";
            if (buildingAdvantageList != null) {
                for (var i = 0; i < buildingAdvantageList.length; i++) {
                    advantage = advantage +
                        "<span>" + buildingAdvantageList[i].advantageName + "</span>"
                }
                $(".subtitle").html(advantage);
            }
            var mySwiper = new Swiper('.swiper-container', {
		         	autoplay:{
				      delay:3000,
				      stopOnLastSlide:false,
				      disableOnInteraction: false
			    },

			     // 如果需要分页器
			        pagination: {
				      el: '.swiper-pagination',
			    },
		 })
     }
    });

    $(".information").click(function () {
        var url = "../build/index.html?id=" + id;
        window.location.href = url;
    });

    $(".fanhui").click(function () {
        history.back();
    });

	
	/*dianhua*/
	
    $(".btn").click(function () {
        $(".ui-circle_phone").attr("href");
        var phone = $(".ui-circle_phone").attr("href");
        sendTel(phone);
    });

    $(".lptel").click(function () {
        $(".lptel").attr("href");
        var phone = $(".lptel").attr("href");
        sendTel(phone);
    });

    $("#phone").click(function () {
        $("#phone").attr("href");
        var phone = $("#phone").attr("href");
        sendTel(phone);
    });
		
		$(".footer").on("click",".footer-coupon",function(){ //给未来元素定义
			var url = "../buyinfo/index.html?id=" + id;
			window.location.href = url;
		});

    function sendTel(phone) {
        $.ajax({
            url: BASE_URL+"/building/telHistory",
            data: {
                "phone": phone
            },
            xhrFields: {
				withCredentials: true
			},
            type: "get",
            dataType: "json",
            success: function (data) {
							
            }
        });
    }	
	
});
