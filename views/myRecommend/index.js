$(function() {
	var dic = [];
	getStateDic(function(res) {
		dic = res;
		getList();
	})
	//初始化函数
	function getList() {
		$.ajax({
			type: "get",
			url: BASE_URL + "/staff/commend",
			data: {

			},
			xhrFields: {
				withCredentials: true
			},
			success: function(data) {
				var html = "";
				var ele = data.data;
				for(var i = 0; i < ele.length; i++) {
					html += [
						//客户姓名信息
						(function() {
							return '<div class="custom-item" id="">' +
								'<span id="">姓名:</span>' +
								'<input type="text" name="name" id="name" value="' + ele[i].realName + '" readonly="readonly"/>' +
								'</div>'
						})(),
						//客户电话
						(function() {
							return '<div class="custom-item" id="">' +
								'<span id="">电话:</span>' +
								'<input type="text" name="phone" id="phone" value="' + ele[i].phone + '" readonly="readonly"/>' +
								'<span id="" class="icon-info">' +
								'<a href="sms:' + ele[i].phone + '"><i class="iconfont icon-xinfeng1"></i></a>' +
								'</span>' +
								'<span id="" class="icon-info">' +
								'<a href="tel://' + ele[i].phone + '"><i class="iconfont icon-dianhua"></i></a>' +
								'</span>' +
								'</div>'
						})(),
						//客户备用电话
						(function() {
							if(ele[i].sparePhone != null){
								return '<div class="custom-item" id="">' +
								'<span id="subNum">备用电话:</span>' +
								'<input type="text" name="standbyPhone" id="standbyPhone" value="' + ele[i].sparePhone + '" readonly="readonly"/>' +
								'<span id="" class="icon-info">' +
								'<a href="sms:' + ele[i].sparePhone + '"><i class="iconfont icon-xinfeng1"></i></a>' +
								'</span>' +
								'<span id="" class="icon-info">' +
								'<a href="tel:' + ele[i].sparePhone + '"><i class="iconfont icon-dianhua"></i></a>' +
								'</span>' +
								'</div>'
							}else{
								return "";
							}
						})(),
						//客户意向楼盘
						(function() {
							return '<div class="custom-item" id="">' +
								'<span id="">意向楼盘:</span>' +
								'<input type="text" name="intent" id="intent" value="' + ele[i].buildingTitle + '" readonly="readonly" />' +
								'</div>'
						})(),
						//客户录入时间
						(function() {
							return '<div class="custom-item" id="">' +
								'<span id="">推荐时间:</span>' +
								'<input type="text" name="inputTime" id="inputTime" value="' + ele[i].createDate + '" readonly="readonly" />' +
								'</div>'
						})(),
						//对客户的备注
						(function() {
							return '<div class="custom-item" id="">' +
								'<span id="">备注:</span>' +
								'<input type="text" name="demand" id="demand" value="' + ele[i].remark + '" readonly="readonly" />' +
								'</div>'
						})(),
						/*//置业顾问
						(function() {
							return '<div class="custom-item" id="">' +
								'<span id="">置业顾问:</span>' +
								'<input type="text" name="guwen" id="guwen" value="' + ele[i].adviserName + '" readonly="readonly" />' +
								'</div>'
						})(),*/
						//顾问成员
						(function() {
							var advisers = ele[i].adviserList;
							var adviserName = '';
							if(data != null){
								for(var j=0;j<advisers.length;j++){
									adviserName += advisers[j].name+",";
								}
								adviserName = adviserName.substring(0,adviserName.length-1);
								return '<div class="custom-item" id="">' +
								'<span id="">置业顾问:</span>' +
								'<input type="text" name="anotherAdviser" id="anotherAdviser" value="' + adviserName+ '" readonly="readonly" />' +
								'</div>'
							}
							
						})(),
						//状态
						(function() {
							return '<div class="custom-item blanking" id="">' +
								'<span id="">状态:</span>' +
								'<div class="state">' +
								getState(ele[i].progressStatus) +
								'</div>' +
								'</div>'
						})(),
					].join('')
				}
				$(".custom-subinfo").html(html);
			}
		});
	}

	//加载字典
	function getStateDic(callback) {
		$.ajax({
			type: "get",
			url: BASE_URL + "/coreDict/getDictByType",
			data: {
				"dictName": "progress_status"
			},
			xhrFields: {
				withCredentials: true
			},
			success: function(data) {
				callback(data.data);
			}
		});

	}
	//获取客户状态
	function getState(state) {
		var str = "",
			flag = false,
			last = false,
			active = ' state-list-act';
		if(state === 0) last = true;
		for(var j = 0; j < dic.length; j++) {
			var ele = dic[j];
			var calc = Number(ele.value) === state;
			str += '<div class="state-list' +
				(function() {
					if(calc && last) return active;
					else if(last) return '';
					else return flag ? '' : active;
				})() +
				'">' +
				'<i class="iconfont icon-yuanquan1"></i>' +
				'<span>' + ele.name + '</span>' +
				'</div>'

			if(calc) flag = true;
		}
		return str;
	}

	$(".xiaoyuhao").click(function() {
		history.back();
	});
})