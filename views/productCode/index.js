$(function() {
	var productId = getUrlParam("id");
	var result;
	$.ajax({
		type: "get",
		url: BASE_URL + "/mall/buyMallProduct",
		data: {
			"id": productId
		},
		async: true,
		dataType: "json",
		xhrFields: {
			withCredentials: true
		},
		success: function(data) {
			console.log(data);
			var html = "";
			var codeStr = "";
			result = data.data.mallInfo;
			var ele = data.data.voucherInfoVOList;
			//赋值对象
			html += [
				//最外层框子
				'<div class="goods-sublist" id="' + result.id + '">',
				//图片信息
				(function() {
					return '<img src="' + IMG_URL + result.productPic + '"/>'
				})(),
				//商品信息
				(function() {
					return '<div class="goods-con">' +
						'<p class="goods-text">' + result.productName + result.productModel + '</p>' +
						'<span>价格：￥' + result.productSalePrice + '</span>' +
						'</div>'

				})(),
				'</div>',
				//购买数量
				(function() {
					return '<div class="buy-text buy-num">' +
						'<div class="num-text">购买数量' +
						'<span>1</span>' +
						'</div>' +
						'</div>'

				})(),
				//配送方式
				(function() {
					return '<div class="buy-text">' +
						'<div class="num-text">配送方式' +
						'<span>线下自提</span>' +
						'</div>' +
						'</div>'

				})(),
				//优惠券选择
				(function() {
					return '<div class="buy-text">' +
						'<div class="num-text">代金券' +
						'<div class="buy-coupon">' +
						'<select name="" class="code-select">' +
						'<option value="0" id="0">无优惠</option>' +
						'</select>' +
						'<a class="item-right">' +
						'<i class="iconfont icon-dayuhao"></i>' +
						'</a>' +
						'</div>' +
						'</div>' +
						'</div>'
				})(),
				(function() {
					return '<div class="buy-code">二维码' +
						'<div class="code-info">' +
						'<img id="code-img" src="' + IMG_URL + result.qrPath + '" />' +
						'<p id="code-num">' + result.qrCode + '</p>' +
						'</div>' +
						'</div>'
				})(),
				(function() {
					return '<div class="buy-text">' +
						'<div class="price-text">' +
						'<div class="code-price">' +
						'<span>优惠金额：</span>' +
						'<span id="codeP">' + 0 + '元</span>' +
						'</div>' +
						'<div class="real-price">' +
						'<span>实付金额：</span>' +
						'<span id="realP">' + result.productSalePrice + '元</span>' +
						'</div>' +
						'</div>' +
						'</div>'

				})(),
				(function() {
					return '<div class="tips">' +
						'<span>请携带身份证进行兑换</span>' +
						'</div>'
				})(),
			].join('')
			$(".goods").html(html);
			if(ele.length != 0) {
				for(var i = 0; i < ele.length; i++) {
					codeStr = codeStr + '<option value="' + ele[i].voucherName + '" id="' + ele[i].id + '">' + ele[i].voucherName + '</option>';
				}
			}
			$(".code-select").append(codeStr);

			$(".code-select").change(function() {
				var codeId = $(".code-select>option:selected").attr("id");
				if(codeId == '0') {
					$("#code-img").attr("src", IMG_URL + result.qrPath);
					$("#code-num").text(result.qrCode);
					$("#codeP").text(0 + "元");
					$("#realP").text(result.productSalePrice + "元");
					$(".tips").css("display", "none");
				} else {
					$.ajax({
						type: "get",
						url: BASE_URL + "/voucherInfo/voucherDetailQuery",
						data: {
							"id": codeId
						},
						async: true,
						dataType: "json",
						xhrFields: {
							withCredentials: true
						},
						success: function(data) {
							var voucherInfo = data.data;
							$("#code-img").attr("src", IMG_URL + voucherInfo.qrPath);
							$("#code-num").text(voucherInfo.qrCode);
							$("#codeP").text(voucherInfo.preferential + "元");
							$("#realP").text(voucherInfo.productPayPrice + "元");
							if(voucherInfo.type == '0') {
								$(".tips").css("display", "block");
							} else {
								$(".tips").css("display", "none");
							}
						}
					});
				}
			})
		}
	});
	$(".xiaoyuhao").click(function() {
		history.back();
	});
})