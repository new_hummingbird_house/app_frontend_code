$(function() {
	var productId = getUrlParam("id");
	$.ajax({
		type: "get",
		url: BASE_URL + "/mall/getMallDetail",
		data: {
			"id": productId
		},
		xhrFields: {
			withCredentials: true
		},
		success: function(data) {
			var html = "";
			var str = "";
			var result = data.data.mallInfo;
			html += [
			//商品图片
				'<img src="' + IMG_URL+ result.productPic + '" />',
				//商品信息
				(function() {
					return '<div class="product-info">' +
						'<p class="goods-text">' + result.productName + '</p>' +
						'<p class="goods-text">型号：' + result.productModel + '</p>' +
						'价格:<span>￥' + result.productSalePrice + '</span>' +
						'</div>' +
						'<div class="product-ads">供货商:' +

						'<input type="text" name="address" id="address" value="' + result.productDealer + '" readonly="readonly"/>' +
						'</div>'
				})(),
				//商品详情图
				(function() {
					return '<h4>商品详情</h4><div class="product-img">' +

					'</div>'
				})(),
			].join('')
			
			str += '<img src="' + IMG_URL+ '/mall/head_mallDetail.jpg' + '" />';
			for(var i = 0; i < data.data.mallInfoDetailList.length; i++) {
				str += '<img src="' + IMG_URL+ data.data.mallInfoDetailList[i].productDetailPic + '" />';
			}
			$(".product").html(html);
			$(".product-img").html(str);
			
//			$(".goods-text").each(function(){
//				var maxwidth = 40;
//				var text = $(this).text();
//				if($(this).text().length>maxwidth){
//					debugger
//					alert(text);
//					$(this).text($(this).text().substring(0,maxwidth));
//              $(this).html($(this).html()+"...");
//				}
//			})
			
		}

	});

	$(".nav-bar-left").click(function() {
		history.back();
	});
	 $(".footer-buy").click(function(){
		var url = "../productCode/index.html?id=" + productId;
		window.location.href = url;
    });
});