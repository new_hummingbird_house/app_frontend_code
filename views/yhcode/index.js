$(function() {
	var id = getUrlParam("id");
	$.ajax({
		url: BASE_URL + "/voucherInfo/voucherDetailQuery",
		data: {
			"id": id
		},
		type: "get",
		dataType: "json",
		xhrFields: {
			withCredentials: true
		},
		success: function(data) {
			var html = "";
			var str = "";
			html += '<img class="img-erweima" src="' + IMG_URL + data.data.qrPath + '">' +
				'<p class="subtitle">' + data.data.qrCode + '</p>';
			$(".list-box").html(html);
			str += '<div class="footer-list">' +
				'<p class="datashow">' +
				'<h3>优惠信息:</h3>' +
				'<span>商品名称：</span>' +
				'<span>' + data.data.productName + '</span></p>' +
				'<p class="datashow">' +
				'<span>商品型号：</span>' +
				'<span>' + data.data.productModel + '</span></p>' +
				'<p class="datashow">' +
				'<span>商品价格：</span>' +
				'<span>' + data.data.productSalePrice + '元</span></p>' +
				'<p class="datashow">' +
				'<span>优惠金额：</span>' +
				'<span>' + data.data.preferential + '元</span></p>' +
				'<p class="datashow">' +
				'<span>实付金额：</span>' +
				'<span>' + data.data.productPayPrice + '元</span></p>' +
				'<span>供 货 商 ：</span>' +
				'<span>' + data.data.productDealer + '</span></p>' +
				'<p class="tips">';
			if(data.data.type == 0) {
				str += '<span>请携带身份证进行兑换</span>' + '</p>' +
					'</div>';
			} else {
				str += '</div>';
			}
			$(".footer-box").html(str);
		}
	});

	$(".fanhui").click(function() {
		history.back();
	});
});