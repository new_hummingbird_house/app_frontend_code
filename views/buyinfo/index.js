$(function() {
	var id = getUrlParam("id");
	$.ajax({
		type: "get",
		url: BASE_URL + "/voucherApplication/getVoucherVO",
		data: {
			"buildingInfoId": id
		},
		async: false,
		dataType: "json",
		xhrFields: {
			withCredentials: true
		},
		success: function(data) {
			var html = "";
			var str = "";
			ele = data.data;
			html += '<div class="item ">' +
				'<span>姓名:</span>' +
				'<input type="text " name="name " id="name" value="' + replaceNull(ele.realName) + '"></input>' +
				'</div>' +
				'<div class="item ">' +
				'<span>电话:</span>' +
				'<input type="text " name="phone " id="phone" value="' + ele.phone + '" readonly="readonly"></input>' +
				'</div>' +
				'<div class="item">' +
				'<span>身份证号:</span>' +
				'<input type="text " name="IDcode " id="IDcode" value="" maxlength="18"></input>' +
				'</div>' +
				'<div class="item">' +
				'<span>购买楼房:</span> 小区名称' +
				'<input type="text" name="community" class="community" id="' + id + '" value="' + ele.buildingTitle + '" readonly="readonly">' +
				'<div class="buyinfo">' +
				'<input type="text" name="2" id="2" value="">号楼' +
				'<input type="text" name="3" id="3" value="">单元' +
				'<input type="text" name="4" id="4" value="">房间' +
				'</div>' +
				'</div>' +
				'<div class="item">' +
				'<span>置业顾问:</span>' +
				'<select name="guwen" id="guwen">' +			
				'</select>' +	
				'</div>' +
				'<div class="item" id="pruduct">' +
				'</div>';

			for(var i = 0; i < data.data.buildingAdviserList.length; i++) {
				str += '<option value="' + data.data.buildingAdviserList[i].id + '">' + data.data.buildingAdviserList[i].name + '</option>';
			};

			$(".contentx").html(html);
			$("#guwen").html(str);		
		}
	});

	$.ajax({
		type: "get",
		url: BASE_URL + "/mall/getMallList",
		data: {
			"type": "",
			"keyWord":"",
			"sys":""
		},
		dataType: "json",
		async: false,
		xhrFields: {
			withCredentials: true
		},
		success: function(data) {
			if(data.code=='200'){
				var mallInfoList = data.data.mallInfoList;
				if(mallInfoList==null){
					alert("获取商品信息失败!");
				}else{
					var str ='<span>商品信息:</span>' +
					'<select name="mallInfo" id="mallInfo">' +	
					'';
					
					for(var i = 0; i < mallInfoList.length; i++){
							str += '<option value="' + 
							mallInfoList[i].id + '">' 
							+ mallInfoList[i].productName + '</option>';
					}		
					str += "</select>";
					$("#pruduct").html(str);				
				}
			}else{
				alert(data.msg);
			}	
		}
	});

	$(".nav-bar-left").click(function() {
		history.back();
	});
	$("#lg-sub-s").click(function() {
		var name = $("#name").val();
		var phone = $("#phone").val();
		var IDcard = $("#IDcode").val();
		var community = $(".community").attr("id");
		var guwen = $("#guwen").val();
		var mallInfoId = $("#mallInfo").val(); 
		var lou = $("#2").val();
		var dan = $("#3").val();
		var fang = $("#4").val();
		var hao = "号楼";
		var yuan = "单元";
		var jian = "房间";
		var roomInfo = lou.concat(hao, dan, yuan, fang, jian);
		if(name == "") {
			alert("姓名不能为空");
			$("#name").focus();
			return false;
		}
		if(IDcard == "") {
			alert("身份证号不能为空");
			$("#IDcode").focus();
			return false;
		}
		if(lou == "") {
			alert("楼号不能为空");
			return false;
		}
		if(dan == "") {
			alert("单元号不能为空");
			return false;
		}
		if(fang == "") {
			alert("房间号不能为空");
			return false;
		}

		$.ajax({
			type: "post",
			url: BASE_URL + "/voucherApplication/add",
			data: {
				"name": name,
				"phone": phone,
				"buildingInfoId": community,
				"apartment": roomInfo,
				"adviserId": guwen,
				"var1": mallInfoId,
				"identityCard": IDcard,
			},
			dataType: "json",
			xhrFields: {
				withCredentials: true
			},
			success: function(data) {
				console.log(data);
				if(data.code == 200) {
					alert(data.msg);
					history.go(-1);
				} else {
					alert(data.msg);
				}
			}
		});
	});

	$(".xiaoyuhao").click(function() {
		history.back();
	});
});
