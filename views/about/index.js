$(function() {
	window.onpageshow = function(event) {
	　　if (event.persisted) {
	　　　　window.location.reload() 
	　　}
	};
	var userName = $("#userInfo").val();
	var userImg = $("#user-img").attr("src");
	$.ajax({
		type: "get",
		url: BASE_URL + "/wode/customer",
		async: true,
		cache: false,
		data: {

		},
		xhrFields: {
			withCredentials: true
		},
		success: function(data) {
			if(data.code == 200) {
				$("#userInfo").val(data.data.customer.loginId);
				$("#user-img").attr("src", IMG_URL + data.data.customer.photoUrl);
				$("#lg-show-yes").removeClass("lg-hide");
				if(data.data.customer.photoUrl == null) {
					$("#lg-show-yes").find("#user-img").addClass("lg-hide");
				} else {
					$("#lg-show-yes").find(".icon-wode").addClass("lg-hide");
				}
				if(data.data.roles.indexOf("dealer") != -1) {
					$(".item-dealer").removeClass("lg-hide");
					$(".itemQuit").removeClass("lg-hide");
					$(".itemReport").click(function() {
						window.location.href = "../baobei/index.html";
					})
					$(".itemCustom").click(function() {
						window.location.href = "../myCustom/index.html";
					})
				}
				if(data.data.roles.indexOf("customer") != -1) {
					$(".item-custom").removeClass("lg-hide");
					$(".itemQuit").removeClass("lg-hide");
				}
				if(data.data.roles.indexOf("agent") != -1) {
					$(".item-agent").removeClass("lg-hide");
					$(".itemQuit").removeClass("lg-hide");
				}
				if(data.data.roles.indexOf("zuoXiao") != -1||data.data.roles.indexOf("xingXiao")!=-1) {
					$(".item-dealer").removeClass("lg-hide");
					$(".itemQuit").removeClass("lg-hide");
					$(".item-employee").removeClass("lg-hide");
					$(".itemReport").click(function() {
						window.location.href = "../baobeiAdviser/index.html";
					})
					$(".itemCustom").click(function() {
						window.location.href = "../adviserCustom/index.html";
					})

				}
			} else {
				$("#lg-show-no").removeClass("lg-hide");
			}
		},
		error:function(e){
			$(".itemQuit").click();
		}
	});
	
	
	
	
	$(".itemCoupon").click(function() {
		window.location.href = "../coupon/index.html";
	});
	$(".itemCommend").click(function() {
		window.location.href = "../tuijian/index.html";
	})

	$(".itemDemand").click(function() {
		window.location.href = "../myRecommend/index.html";
	})
	$(".itemScan").click(function() {
		window.location.href = "../saosao/index.html";
	})
	$(".itemVerify").click(function() {
		window.location.href = "../verify/index.html";
	})
	$(".itemNeed").click(function() {
		window.location.href = "../customInfo/index.html";
	})
	$(".itemQuit").click(function() {
		$.ajax({
			type: "get",
			url: BASE_URL + "/wode/quitLogin",
			async: true,
			data: {

			},
			xhrFields: {
				withCredentials: true
			},
			success: function(data) {
				if(data.code == 200) {
					location.reload();
				}
			}
		});
	});
	$(".xiaoyuhao").click(function() {
		history.back();
	});
})