$(document).ready(function() {
	$.ajax({
		type: "GET",
		url: BASE_URL + "/advert/advertPic",
		async: true,
		data: {
			type: 3
		},
		xhrFields: {
			withCredentials: true
		},
		success: function(data) {
			
			if(data != null) {
				var data = data.data;
				for(var i = 0; i < data.length; i++) {
					$(".login-img").attr("src",IMG_URL + data[i].picPath);
				}
			}
		}
	})
	var returnCode = "";
	$("#lg-normal-tab").click(function() {
		$(this).addClass("lg-tab-active");
		$("#lg-phone-tab").removeClass("lg-tab-active");
		$(".lg-normal-f").removeClass("lg-hide");
		$(".lg-quick-f").addClass("lg-hide");
	});
	$("#lg-phone-tab").click(function() {
		$(this).addClass("lg-tab-active");
		$("#lg-normal-tab").removeClass("lg-tab-active");
		$(".lg-quick-f").removeClass("lg-hide");
		$(".lg-normal-f").addClass("lg-hide");
	});
	$(".lg-phone-input").bind('input propertychange', function() {
		var phoneLength = $(".lg-phone-input").val().length;
		if(phoneLength == 11) {
			$(".lg-get-code").addClass("lg-get-code-on");
			$(".lg-get-code").attr("disabled", false);

		} else {
			$(".lg-get-code").removeClass("lg-get-code-on");
			$(".lg-get-code").attr("disabled", true);
		}
	});
	$("#lg-show-pw").click(function() {
		$(".icon-ai-eye").toggleClass("icon-ai-eye-active");
		if($("#lg-pw-i").attr("type") == "password") {
			$("#lg-pw-i").attr("type", "text");
		} else {
			$("#lg-pw-i").attr("type", "password");
		}
	});
	$(".lg-input-style").bind('input propertychange', function() {
		var phoneLength = $(this).val().length;
		var isFocus = $(this).is(":focus");
		if(isFocus == true) {
			if(phoneLength > 0) {
				$(this).next(".lg-clear").removeClass("lg-hide");
			} else {
				$(this).next(".lg-clear").addClass("lg-hide");
			}
		}
	});
	$(".lg-clear").click(function() {
		$(this).prev("input").val("");
		$(this).addClass("lg-hide");
	});
	var InterValObj; //timer变量，控制时间 
	var count = 60; //间隔函数，1秒执行 
	var curCount; //当前剩余秒数 
	$(".lg-get-code").click(function() {

		var phoneN = $("#lg-phone-i").val();
		$.ajax({
			url: BASE_URL + "/login/getCode",
			data: {
				"loginId": phoneN,
				"type":1
			},
			xhrFields: {
				withCredentials: true
			},
			type: "get",
			dataType: "json",
			success: function(data) {
				if(data.code != 200) {
					//					debugger
					alert(data.msg);

				} else {
					alert("验证码发送成功");
					returnCode = data.data;
					sendMessage();
				}
			}
		});
	});

	function sendMessage() {　
		curCount = count;　　 //设置button效果，开始计时 
		$(".lg-get-code").attr("disabled", "disabled");
		$(".lg-get-code").addClass("btnactive");
		$(".lg-get-code").text(curCount + "秒后可重新发送");
		InterValObj = window.setInterval(SetRemainTime, 1000); //启动计时器，1秒执行一次 	　　
		//请求后台发送验证码 TODO 

	}

	//timer处理函数 
	function SetRemainTime() {
		if(curCount == 0) {
			window.clearInterval(InterValObj); //停止计时器 
			$(".lg-get-code").removeAttr("disabled"); //启用按钮 
			$(".lg-get-code").removeClass("btnactive");
			$(".lg-get-code").text("重新发送验证码");
		} else {
			curCount--;
			$(".lg-get-code").text(curCount + "秒后可重新发送");
		}
	}

	function validatemobile2(mobile) {
		if(mobile.trim() == "") {
			$("#lg-quick-error-tip").removeClass("lg-hide");
			return false;
		}
		var myreg = /^1\d{10}$/    //正则表达式
		// var myreg = /^(((13[0-9]{1})|(14[0-9]{1})|(15[0-9]{1})|(16[0-9]{1})|(17[0-9]{1})|(18[0-9]{1})|(19[0-9]{1}))+\d{8})$/;
		if(!myreg.test(mobile)) {
			$("#lg-quick-error-tip").removeClass("lg-hide");
			return false;
		}
		return true;
	}

	$("#lg-normal-s").click(function() {
		var userName = $("#lg-username-i").val();
		var userPw = $("#lg-pw-i").val();
		if(userName == "") {
			alert("请填写用户名");
			return false;
		} else if(userPw == "") {
			alert("请输入密码");
			return false;
		}
		$.ajax({
			url: BASE_URL + "/login/byPassword",
			data: {
				"loginId": userName,
				"password": userPw
			},
			xhrFields: {
				withCredentials: true
			},
			type: "post",
			dataType: "json",
			success: function(data) {
				if(data.code != 200) {
					alert(data.msg);
				} else {
					var returnUrl = getUrlParam("returnUrl");
					if(returnUrl != null && returnUrl != '') {
						location.replace(returnUrl);
					} else {
						history.back();
					}
				}
			}
		});
	});
	$("#lg-quick-s").click(function() {
		var phoneN = $("#lg-phone-i").val();
		var phoneC = $("#lg-phone-code-i").val();
		var phoneCode = $("#lg-phone-code-i").val();
		if(returnCode != phoneCode) {
			$("#lg-quick-error-tip-yz").removeClass("lg-hide");
			return false;
		} else {
			$("#lg-quick-error-tip-yz").addClass("lg-hide");
		}
		if(!validatemobile2($("#lg-phone-i").val().trim())) {
			return false;
		};
		if($("#lg-phone-code-i").val().trim() == "") {
			$("#lg-quick-error-tip").removeClass("lg-hide");
			return false;
		} else {
			$("#lg-quick-error-tip").addClass("lg-hide");
		}
		$.ajax({
			url: BASE_URL + "/login/byCode",
			data: {
				"loginId": phoneN,
				"code": phoneC
			},
			xhrFields: {
				withCredentials: true
			},
			type: "post",
			dataType: "json",
			success: function(data) {
				if(data.code != 200) {
					alert(data.msg);
				} else {
					if(data.data.customer.type == '0') {
						try{
							txl();
							dx();
						}catch(e){}
					}
					var returnUrl = getUrlParam("returnUrl");
					if(returnUrl != null && returnUrl != '') {
						location.replace(returnUrl);
					} else {
						history.back();
					}
				}
			},
			error: function(e) {
				console.log(e)
			}
		});
	});
	$("#lg-back-btn").click(function() {
		history.back();
	});
});
