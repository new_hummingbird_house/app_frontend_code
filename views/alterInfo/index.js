$(function() {
	var adviserCusId = getUrlParam("id");
	var sourceType = getUrlParam("sourceType");

	$.ajax({
		type: "get",
		url: BASE_URL + "/employee/follow",
		data: {
			"id": adviserCusId,
			"sourceType": sourceType
		},
		xhrFields: {
			withCredentials: true
		},
		success: function(data) {
			var html = "";
			var ele = data.data;
			html += [
				//客户姓名信息
				(function() {
					return '<div class="adviser-custom">' +
						'<div class="custom-item" id="' + ele.sourceType + '">' +
						'<span id="">姓名:</span>' +
						'<input type="text" name="name" id="name" value="' + ele.realName + '" readonly="readonly"/>' +
						'</div>'
				})(),

				//客户电话
				(function() {
					return '<div class="custom-item" id="">' +
						'<span id="">电话:</span>' +
						'<input type="text" name="phone" id="phone" value="' + ele.phone + '" readonly="readonly"/>' +
						'<span id="" class="icon-info">' +
						'<a href="sms:' + ele.phone + '"><i class="iconfont icon-xinfeng1"></i></a>' +
						'</span>' +
						'<span id="" class="icon-info">' +
						'<a href="tel:' + ele.phone + '"><i class="iconfont icon-dianhua"></i></a>' +
						'</span>' +
						'</div>'
				})(),
				//客户备用电话
				(function() {
					if(ele.sparePhone != null){
								return '<div class="custom-item" id="">' +
								'<span id="subNum">备用电话:</span>' +
								'<input type="text" name="standbyPhone" id="standbyPhone" value="' + ele.sparePhone + '" readonly="readonly"/>' +
								'<span id="" class="icon-info">' +
								'<a href="sms:' + ele.sparePhone + '"><i class="iconfont icon-xinfeng1"></i></a>' +
								'</span>' +
								'<span id="" class="icon-info">' +
								'<a href="tel:' + ele.sparePhone + '"><i class="iconfont icon-dianhua"></i></a>' +
								'</span>' +
								'</div>'
							}else{
								return "";
							}
				})(),
				//推送人姓名
				(function() {
					if(sourceType == 1) {
						return '<div class="custom-item" id="">' +
							'<span id="">报备人:</span>' +
							'<input type="text" name="adviser" id="adviser" value="' + ele.customerName + '" readonly="readonly"/>' +
							'<span id="" class="icon-info">' +
							'<a href="sms:' + ele.adviserPhone + '"><i class="iconfont icon-xinfeng1"></i></a>' +
							'</span>' +
							'<span id="" class="icon-info">' +
							'<a href="tel:' + ele.adviserPhone + '"><i class="iconfont icon-dianhua"></i></a>' +
							'</span>' +
							'</div>'
					} else {
						return '<div class="custom-item" id="">' +
							'<span id="">推荐人:</span>' +
							'<input type="text" name="commend" id="commend" value="' + ele.customerName + '" readonly="readonly"/>' +
							'<span id="" class="icon-info">' +
							'<a href="sms:' + ele.adviserPhone + '"><i class="iconfont icon-xinfeng1"></i></a>' +
							'</span>' +
							'<span id="" class="icon-info">' +
							'<a href="tel://' + ele.adviserPhone + '"><i class="iconfont icon-dianhua"></i></a>' +
							'</span>' +
							'</div>'
					}

				})(),
				//客户意向楼盘
				(function() {
					return '<div class="custom-item" id="">' +
						'<span id="">意向楼盘:</span>' +
						'<input type="text" name="intent" id="intent" value="' + ele.buildingTitle + '" readonly="readonly" />' +
						'</div>'
				})(),
				//客户录入时间
				(function() {
					return '<div class="custom-item" id="">' +
						'<span id="">报备时间:</span>' +
						'<input type="text" name="inputTime" id="inputTime" value="' + ele.createDate + '" readonly="readonly" />' +
						'</div>'
				})(),
				//对客户的报备留言
				(function() {
					return '<div class="custom-item" id="">' +
						'<span id="">报备留言:</span>' +
						'<input type="text" name="demand" id="demand" value="' + ele.remark + '" readonly="readonly" />' +
						'</div>'
				})(),
				//置业顾问
				/*(function() {
					return '<div class="custom-item" id="">' +
						'<span id="">置业顾问:</span>' +
						'<input type="text" name="guwen" id="guwen" value="' + ele.adviserName + '" readonly="readonly" />' +
						'</div>'
				})(),*/
				//顾问成员
				(function() {
					if(ele.adviserMember != null){
						return '<div class="custom-item" id="">' +
						'<span id="">置业顾问:</span>' +
						'<input type="text" name="anotherAdviser" id="anotherAdviser" value="' + ele.adviserMember+ '" readonly="readonly" />' +
						'</div>'
					}
					
				})(),
				//当前状态
				(function() {
					return '<div class="custom-item" id="">' +
						'<span id="">当前状态:</span>' +
						'<select name="progressStatus" id="progressStatus">' +
						'<option value="' + data.data.id + '" >' + data.data.progressStatusText + '</option>' +
						'</select>' +
						'</div>' +
						'</div>'
				})(),
				//状态
				(function() {
					if(data.data.progressStatus != 0){
						return '<div class="custom-item" id="">' +
						'<span id="">状态修改:</span>' +
						'<select name="state" id="state">' +

						'</select>' +
						'</div>' +
						'</div>'
					}
					
				})(),
				//顾问修改状态留言
				(function() {
					if(data.data.progressStatus != 0){
						return '<div class="custom-item" id="">' +
						'<span id="">状态留言:</span>' +
						'<input type="text" name="stateText" id="stateText" value="" />' +
						'</div>'
					}
				})(),
			].join('')
			$(".custom-subinfo").html(html);
			
			var str = "";
			var result = data.data.nextStatus;
			for(var i = 0; i < result.length; i++) {
				str += '<option value="' + result[i].value + '">' + result[i].name + '</option>';
			}
			$("#state").append(str);
			
			if(data.data.progressStatus != 0){
				$(".lg-sub").removeClass("lg-hide");
			}
		}
	});
	$("#lg-sub-s").click(function() {
		var updateStatus = $("#state").val();
		var content = $("#stateText").val();
		$.ajax({
			type: "put",
			url: BASE_URL + "/employee/progress",
			data: {
				"sourceType": sourceType,
				"sourceId": adviserCusId,
				"updateStatus": updateStatus,
				"content": content
			},
			dataType: "json",
			xhrFields: {
				withCredentials: true
			},
			success: function(data) {
				if(data.code == 200) {
					alert(data.msg);
					history.go(-1);
				} else {
					alert(data.msg);
				}
			}
		});
	})

	$(".xiaoyuhao").click(function() {
		history.back();
	});

	function getUrlParam(name) {
		var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
		var r = window.location.search.substr(1).match(reg); //匹配目标参数
		if(r != null) return unescape(r[2]);
		return null; //返回参数值
	}
})